if [ -z "$3" ]; then
    echo "No .txt filename containing the coefficient matrix supplied. Please specify a file name.";
fi

# Example of input file name: Altshuler_M963_deg3_matrix.txt
node_method=${1:-0}
print_nonzero_dual_vars=${2:-"no"}
fname=$3
diary_name=$4
sleep_amount=${5:-5}

if [ ! -f "$fname" ]; then
    fname=`echo $fname | sed -e "s|matrices/||g"`
fi

case "$node_method" in
    -1)
        echo "Method: automatic";;
    0)
        echo "Method: primal simplex";;
    1)
        echo "Method: dual simplex";;
    2)
        echo "Method: barrier";;
    3)
        echo "Method: concurrent";;
    4)
        echo "Method: deterministic concurrent";;
    5)
        echo "Method: deterministic concurrent simplex";;
    *)
        echo "Method should be an integer between -1 and 5."
        exit 1 
esac

prefix=`echo $fname | sed -e "s|matrices/||g" | sed -e "s|_matrix.txt||g"`
m_script_name=$prefix".m"
log_fname="gurobi_logs_"$prefix"_gurobi.txt"

if [ -f "$log_fname" ]; then
    rm $log_fname
fi
if [ -z $diary_name ]; then
    full_diary_name=$log_fname
else
    full_diary_name=$diary_name
fi

echo "Sleeping for "$sleep_amount" seconds..."
sleep $sleep_amount
echo "disp('Loading constraint matrix...')" > $m_script_name
echo "matrix = load('"$fname"');" >> $m_script_name
echo "disp('done.')" >> $m_script_name

echo "r = max(matrix(1, :));" >> $m_script_name
echo "c = max(matrix(2, :));" >> $m_script_name
echo "B = sparse(matrix(1, :), matrix(2, :), matrix(3, :));" >> $m_script_name
echo "D = [B, sparse(ones(r, 1))]';" >> $m_script_name

# Coefficient matrix
echo "model.A = D;" >> $m_script_name

# Lower and upper bound on the variables
echo "model.lb = repmat(-Inf, 1, r);" >> $m_script_name
echo "model.ub = [zeros(1, r)];" >> $m_script_name

# Right-hand side of constraints
echo "model.rhs = [zeros(c, 1); -1];" >> $m_script_name

# Direction of inequalities
echo "model.sense = [repmat('=', 1, c), '>'];" >> $m_script_name

# Objective function
echo "obj_func = full(ones(r, 1));" >> $m_script_name
echo "model.obj = obj_func;" >> $m_script_name
#constant term of the objective function
echo "model.objcon = 1;" >> $m_script_name

echo "model.modelsense = 'min';" >> $m_script_name
echo "params.FeasibilityTol = 10^-9;" >> $m_script_name

# Choosing primal simplex method (other options: -1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 
# 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex)
echo "params.Method = "$node_method";" >> $m_script_name

echo "disp('Starting gurobi solver with parameters:')" >> $m_script_name
echo "params" >> $m_script_name

echo "diary "$full_diary_name >> $m_script_name
echo "result = gurobi(model, params);" >> $m_script_name

echo "try" >> $m_script_name
echo "    sprintf('Objective value: %f', result.objval)" >> $m_script_name

if [ $print_nonzero_dual_vars = "yes" ]; then
    # Indices of nonzero dual variables
    echo "    sprintf('Indices of nonzero dual variables:')" >> $m_script_name
    echo "    nonzero_dual_vars = find(result.x)" >> $m_script_name
    # Submatrix of nonzero primal constraints
    echo "    sparseSubD = D(1:end-2, nonzero_dual_vars);" >> $m_script_name
    echo "    [i, j, s] = find(sparseSubD);" >> $m_script_name
    echo "    sprintf('Column indices:');" >> $m_script_name
    echo "    mons_indices = [unique(i)', c]" >> $m_script_name
    echo "    sprintf('Submatrix of nonzero primal constraints:')" >> $m_script_name
    echo "    fullSubD = [full(sparseSubD(unique(i), :))', full(D(end-1, nonzero_dual_vars))']" >> $m_script_name
    # Sum of nonzero primal constraints
    echo "    sprintf('Sum of nonzero primal constraints:')" >> $m_script_name
    echo "    sum(fullSubD, 1)" >> $m_script_name
    echo "    dlmwrite('"$prefix"_certificate.txt', [mons_indices; fullSubD], 'delimiter', ' ')" >> $m_script_name

    echo "    fileID = fopen('"$prefix"_certificate_rows.txt','w');" >> $m_script_name
    echo "    fprintf(fileID,'%d ', nonzero_dual_vars');" >> $m_script_name
    echo "    fclose(fileID);" >> $m_script_name
fi

# Slacks
echo "    slacks = result.slack;" >> $m_script_name
echo "    if any(slacks == 0) sprintf('There are zero slacks'), else sprintf('There are NO zero slacks.'), end;" >> $m_script_name
echo "    sprintf('Objective value: %f', result.objval)" >> $m_script_name
echo "catch" >> $m_script_name
echo "    warning('Gurobi did not return objval, slack.');" >> $m_script_name
echo "end" >> $m_script_name
echo "diary off" >> $m_script_name

sh run_matlab.sh $m_script_name

rm $m_script_name 2> /dev/null
