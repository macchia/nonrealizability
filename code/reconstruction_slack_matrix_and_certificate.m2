loadPackage("SlackIdeals", Reload => true);

-------------------------------------------------------------------------------------------------------------------------------
-- METHODS TO PRODUCE AN ORIENTATION OF THE FACETS AND PRODUCE THE CONSTRAINTS IN THE RECONSTRUCTED SLACK MATRIX

satPolyPair = f -> (
    -- INPUT: polynomial f
    -- OUTPUT: f divided by any (positive) monomial factors
    -- Note: the constant factor still appears in the result
    p := apply(toList factor(f), e -> value e);
    monomialPart := product select(p, q -> #terms(q) == 1 and degree(q) != {0});
    simplifiedF := product select(p, q -> #terms(q) > 1 or degree(q) == {0});
    return (monomialPart, simplifiedF);
);


satPoly = (f, coordPairList, sign) -> (
    -- INPUT: polynomial f
    -- OUTPUT: f divided by any (positive) monomial factors
    -- Note: the constant factor still appears in the result
    p := apply(toList factor(f), e -> value e);

    monomialPart := product select(p, q -> #terms(q) == 1 and degree(q) != {0});
    simplifiedF := product select(p, q -> #terms(q) > 1 or degree(q) == {0});
    return ({monomialPart, coordPairList, sign}, simplifiedF);
);


findSign = (g, posSat) -> (
    -- INPUT: col = list of polynomials (in a certain column of slack matrix)
    --     posSat = list of polynomials already determined to be positive
    -- OUTPUT: if there exist g in col and p in posSat such that g % p == 0
    --     and g//p is a monomial, it returns the sign of g//p; otherwise it returns 0
    for p in posSat do (
        quot := g // p;
        if (g % p == 0) and (#terms(quot) == 1) then return (satPoly(quot, null, null))_1;
    );
    return 0;
);


findFirstSign = (col, posSat) -> (
    -- INPUT: col = list of polynomials (in a certain column of slack matrix)
    --     posSat = list of polynomials already determined to be positive
    -- OUTPUT: if there exist g in col and p in posSat such that g % p == 0
    --     and g//p is a monomial, it returns the sign of g//p; otherwise it returns 0
    for g in col do (
        for p in posSat do (
            quot := g // p;
            if (g % p == 0) and (#terms(quot) == 1) then return (satPoly(quot, null, null))_1;
        );
    );
    return 0;
);


posCheck = method(Options => {DeepSignCheck => false})
posCheck (HashTable, List, HashTable, List) := (List, HashTable, List, List) => opts -> (hashM, restCols, posSatHash, orientation) -> (
    -- INPUT: hashM = hashTable whose keys are pairs of indices of a matrix M
    --                and values are nonzero entries of M
    --     restCols = columns left to check for sign
    --     posSatHash = hash table of polynomials already determined to be positive
    --     orientation = list containing facets orientation (as pairs (col index, sign +1/-1))
    -- OUTPUT: restCols = columns whose sign are left to determine
    --    posPolys = list of polynomials entries determined to be > 0)
    --    posC = list of columns whose sign we determined
    --    orientation = updated orientation list
    posC := {};

    for c in restCols do (
        hashColC := applyPairs(hashM, (k, v) -> if k_1 == c then (k, v));
        signC := {};

        if opts.DeepSignCheck then (
            signC = unique apply(values hashColC, g -> findSign(g, unique(values(posSatHash))));
            if isSubset({1, -1}, apply(signC, z -> substitute(z, ZZ))) then (
                print("Conflicting signs: " | toString signC);
                error("\nFound incompatible signs in column " | toString c);
            );
        ) else signC = {findFirstSign(values hashColC, unique(values(posSatHash)))};

        if signC == {0} then continue;

        uniqueSignC := (signC - set({0}))_0;
        orientation = orientation | {(c, uniqueSignC)};

        -- otherwise append index c to the list of columns for which we set the sign
        posC = append(posC, c);

        -- we add to posSatHash the unique elements in posPolyC multiplied by signC and divided by possible monomial factors
        posSatHash = merge(
            posSatHash, 
            applyPairs(hashColC, (k, v) -> satPoly(uniqueSignC * v, {k}, uniqueSignC)), 
            (x, y) -> x
        );
    );
    restCols = restCols - set(posC);
    return (restCols, posSatHash, posC, orientation);
);


hashTableFromMatrix = M -> (
    -- INPUT: M = matrix
    -- OUTPUT: hash table whose elements are (i,j) => M_(i,j)
    (r, c) := (numgens target M, numgens source M);
    matrixIndices := apply(toList(0..r-1) ** toList(0..c-1), x->toSequence(x));
    -- return new HashTable from apply(matrixIndices, a -> (a => M_a));
    return new HashTable from apply(matrixIndices, ij -> if M_ij != 0 then (ij => M_ij));
);


getFbases = method(Options => {RedundantNonsimplicialFacets => false})
getFbases (Sequence, String) := List => opts -> (data, fileName) -> (
    -- INPUT: data = (F, d, fl, R, S, S1), where
    --        F = list of facets
    --        d = dimension
    --        fl = a flag of facets
    --        R = polynomial ring where slack matrix lives
    --        S = slack matrix
    --        S1 = reduced scaled slack submatrix corresponding to fl
    --        fileName = string, name of file
    -- OUTPUT: Fbases = list of facet bases
    --         M = reconstructed slack matrix
    (F, d, fl, R, S, S1) := data;

    hashS := hashTableFromMatrix S;
    posNonzeroS := keys hashS;

    -- Find out which sets of vertices span the nonsimplicial facets in our choice of flag
    nonsimplicialFacets := select(F, f -> #f != d);
    print("Nonsimplicial facets : " | toString(nonsimplicialFacets));
    numNonsimplFacets := #nonsimplicialFacets;
    indNonsimplFacets := apply(nonsimplicialFacets, f -> position(F, e -> e==f));

    Fbases := new MutableList from F;
    if numNonsimplFacets != 0 then (
        -- Form slack and reduced slack matrix of sphere
        transposeS := transpose S;

        basesNonsimplFacets := {};
        print("Computing facet bases");
        elapsedTime if not opts.RedundantNonsimplicialFacets then (
            for i to numNonsimplFacets-1 do (
                basesFi := {};
                subsetsFi := subsets(F_(position(F, j -> j == nonsimplicialFacets_i)), d);
                for s in subsetsFi do (
                    try rfl = findFlag(transposeS_s) else continue;
                    -- collect bases of F_i
                    if #rfl == d then (
                        basesFi = s;
                        break;
                    );
                );
                if basesFi === {} then (
                    error("No basis found for nonsimplicial facet " | toString(nonsimplicialFacets_i));
                );
                basesNonsimplFacets = basesNonsimplFacets | {basesFi};
            );
            print("Bases of nonsimplicial facets : " | toString(basesNonsimplFacets));

            for j to numNonsimplFacets-1 do (
                Fbases#(indNonsimplFacets_j) = basesNonsimplFacets_j;
            );

        ) else ( -- adding redundant columns, one for each facet basis of a nonsimplicial facet
            for i to numNonsimplFacets-1 do (
                basesFi := {};
                subsetsFi := subsets(F_(position(F, j -> j == nonsimplicialFacets_i)), d);
                for s in subsetsFi do (
                    try rfl = findFlag(transposeS_s) else continue;
                    -- collect bases of F_i
                    if #rfl == d then (
                        basesFi = basesFi | {s};
                    );
                );
                if basesFi === {} then (
                    error("No basis found for nonsimplicial facet " | toString(nonsimplicialFacets_i));
                );
                basesNonsimplFacets = basesNonsimplFacets | {basesFi};
            );

            for j to numNonsimplFacets-1 do (
                Fbases#(indNonsimplFacets_j) = (basesNonsimplFacets_j)_0;
                Fbases = join(Fbases, (basesNonsimplFacets_j)_{1..#basesNonsimplFacets_j-1});
            );
            print("Total number of columns including redundant ones: " | toString(#Fbases));
        );

    ) else (
        print("\nSimplicial sphere");
    );

    Fbases = toList(Fbases);

    f := fileName | "_Fbases.txt" << "";
    f << toString(for i to #Fbases-1 list (i, Fbases_i)) << endl;
    f << close;

    g := fileName | "_reducedSlackMatrix.txt" << "";
    g << toString(entries S1) << endl;
    g << close;

    return Fbases;
);


colSigns = method(Options => {Oriented => false, SmallDeterminants => false, RedundantNonsimplicialFacets => false, DeepSignCheck => false})
colSigns (Sequence, String) := List => opts -> (data, fileName) -> (
    -- INPUT: data = (R, fl, S1, Fbases) where
    --        R = polynomial ring where slack matrix lives
    --        Fbases = list of facet bases
    --        S1 = reduced slack matrix
    --        fileName = string, name of file
    -- OUTPUT: list posSat of polynomial entries in the reconstructed matrix with correct signs
    (R, fl, S1, Fbases) := data;
    numVerts := numgens target S1;
    
    -- Reconstruct matrix from facet bases
    print("Reconstructing slack matrix");
    elapsedTime (
        M := matrix for i to numVerts-1 list for b in Fbases list det(S1^(join(b, {i})));
        M = substitute(M, R);
    );

    -- load orientation if given
    orientation := {};
    if opts.Oriented then (
        if opts.RedundantNonsimplicialFacets then (
            orientation = value get(fileName |"_orientation_redundant_facets.txt");
        ) else orientation = value get(fileName |"_orientation.txt");
    );

    restCols := toList(0..#Fbases-1);

    -- hashTable whose keys are pairs of indices and values are nonzero entries of M
    hashM := hashTableFromMatrix M;

    posSatHash := new HashTable from {};

    print("\nCheck for reconstructed columns containing monomials.");
    if opts.Oriented then (
        elapsedTime for c in restCols do (
            hashColC := applyPairs(hashM, (k, v) -> if k_1 == c then (k, v));

            mons := applyPairs(hashColC, (k, v) -> if #terms(v) == 1 then (k, v));
            -- if there are no monomials in the column c, then check another column
            if mons === hashTable {} then continue;

            -- compute signs of monomials and check they have same sign
            uniqueSigns := unique apply(values mons, g -> (satPoly(g, null, null))_1);
            if not(all(uniqueSigns, x -> substitute(x, ZZ) > 0) or all(uniqueSigns, x -> substitute(x, ZZ) < 0)) then (
                print("Conflicting signs: " | toString uniqueSigns);
                error("\nFound incompatible signs (from monomials) in column " | toString c);
            );

            -- sign of column c in given orientation
            signC := (orientation_c)_1;
            if uniqueSigns_0 != signC then (
                print("Switching signs to the input orientation");
                orientation = apply(orientation, s -> (s_0, -s_1));
            );

            break;
        );

        posSatHash = applyPairs(hashM, (k, v) -> satPoly((orientation_(k_1))_1 * v, {k}, (orientation_(k_1))_1));
    ) else (
        orientation = {};
        elapsedTime for c in restCols do (
            hashColC := applyPairs(hashM, (k, v) -> if k_1 == c then (k, v));

            mons := applyPairs(hashColC, (k, v) -> if #terms(v) == 1 then (k, v));
            -- if there are no monomials in the column c, then check another column
            if mons === hashTable {} then continue;

            -- compute signs of monomials and check they have same sign
            uniqueSigns := unique apply(values mons, g -> (satPoly(g, null, null))_1);
            if not(all(uniqueSigns, x -> substitute(x, ZZ) > 0) or all(uniqueSigns, x -> substitute(x, ZZ) < 0)) then (
                print("Conflicting signs: " | toString uniqueSigns);
                error("\nFound incompatible signs (from monomials) in column " | toString c);
            );
            orientation = orientation | {(c, uniqueSigns_0)};

            -- we get here if all monomials have the same sign
            restCols = restCols - set({c});

            posSatHash = merge(
                posSatHash, 
                applyPairs(hashColC, (k, v) -> satPoly(uniqueSigns_0 * v, {k}, uniqueSigns_0)),
                (x, y) -> x
            );
        );
        print("Indices of columns containing monomials: " | toString(toList(0..#Fbases-1) - set(restCols)));
        print("Indices of columns with no monomials: " | toString restCols);

        print("\nTest remaining reconstructed columns against polynomials in posSat.");

        --Test remaining reconstructed columns for elements that now already occur in the positive polynomial list
        --Add these entries with appropriate sign to list of positive polynomials
        posC := {-1};
        elapsedTime while restCols != {} and posC != {} do (
            (restCols, posSatHash, posC, orientation) = posCheck(
                hashM, restCols, posSatHash, orientation, 
                DeepSignCheck => opts.DeepSignCheck
            );
            print("List of columns left to fix: " | toString restCols);
        );

        print("\nOrientation : " | toString(sort orientation));
        orientationFileName := fileName | "_orientation";
        if opts.RedundantNonsimplicialFacets then (
            orientationFileName = orientationFileName | "_redundant_facets.txt";
        ) else orientationFileName = orientationFileName | ".txt";
        print("Dumping orientation to file " | orientationFileName);
        g := orientationFileName << "";
        g << toString(sort orientation) << endl;
        g << close;

        if #restCols > 0 then print("Warning: restCols is non-empty after fixing the column signs.");

        facetsWithSign := sort for i to #Fbases-1 list (Fbases_i, (orientation_i)_1);
        print("Facets with sign : " | toString facetsWithSign);
    );

    -- selecting constraints contained in the flag columns divided by monomial factors
    print("\nRemultiplying by monomial factors.");
    if opts.SmallDeterminants then (
        posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_1, fl) then ({1_R, k_1, k_2}, v) else ({1_R, k_1, k_2}, k_0*v));
    ) else posSatHash = applyPairs(posSatHash, (k, v) -> ({1_R, k_1, k_2}, k_0*v));

    print("\nTotal time for colSigns:");
    return posSatHash;
);

------------------------------------------------------------------------------------------------------------------------------------
-- MATRIX OF COEFFICIENTS OF LINEARIZED CONSTRAINTS

getConstraints = method(Options => {Oriented => false, SmallDeterminants => false, RedundantNonsimplicialFacets => false, SwitchToSage => false, DeepSignCheck => false})
getConstraints (Sequence, String) := (List, HashTable) => opts -> (data, fileName) -> (
    -- INPUT: sequence data = (F, d, fl, S1), where list F of facets of a configuration as sets of vertex labels; integer d representing the dimension of given configuration; list fl of integers representing a flag of facets for given configurations; matrix S1 a dehomogenized reduced slack matrix of given configuration;
    -- OUTPUT: list posSat of constraints
    (F, d, fl, S1) := data;

    if #fl != d + 1 then error("Given flag does not have d + 1 elements!");

    S := symbolicSlackMatrix(F, Object => "abstractPolytope");
    R := ring S;

    print("Reduced slack matrix S1 : " | toString S1);
    varsS1 := (unique flatten entries S1 - set{0_(ring S1), 1_(ring S1)});
    print("Variables in S1 : " | toString varsS1 );
    print("Number of variables in S1 : " | toString(#varsS1));

    print("\n---- CONSTRUCTING LIST OF CONSTRAINTS ----");
    elapsedTime Fbases := getFbases(
        (F, d, fl, R, S, S1), fileName,
        RedundantNonsimplicialFacets => opts.RedundantNonsimplicialFacets
    );

    if opts.SwitchToSage then return (Fbases, hashTable {});

    elapsedTime posSatHash := colSigns(
        (R, fl, S1, Fbases), fileName,
        Oriented => opts.Oriented, SmallDeterminants => opts.SmallDeterminants, 
        RedundantNonsimplicialFacets => opts.RedundantNonsimplicialFacets,
        DeepSignCheck => opts.DeepSignCheck
    );

    return (Fbases, posSatHash);
);


getFullProducts = (C, nIters) -> (
    -- INPUT: hash table C whose keys are polynomial constraints and whose values
    --     are lists containing the position (i,j) of a polynomial in the slack matrix;
    --     integer nIters, number of iterations to compute products;
    -- OUTPUT: hash table newC whose keys are (products of) polynomial constraints and
    --     whose values are lists containing the positions (i,j) of the polynomials in
    --     the slack matrix whose product is the key

    if not member(nIters, {1,2,3,4,5}) then error("nIters > 5 not supported.");

    -- first factor
    if nIters == 1 then return C;

    -- second factor
    keysC := keys C;
    listC1x1 := for j in (0..#C-1) list apply(keysC_{0..j}, Ci -> Ci * keysC_j => {(C#Ci)_0 * (C#(keysC_j))_0, (C#Ci)_1 | (C#(keysC_j))_1, (C#Ci)_2 * (C#(keysC_j))_2});
    C1x1 := hashTable flatten listC1x1;
    print("#products of 2 constraints: " | toString(#C1x1));

    -- merging the two hash tables (if two keys are equal, we keep the value of the key in C)
    newC := merge(C, C1x1, (x, y) -> x);
    if nIters == 2 then return newC;

    -- third factor
    keysC1x1 := keys C1x1;
    listC1x1x1 := for k in (0..#C-1) list (flatten apply(listC1x1_{0..k}, C1x1j -> apply(C1x1j, p -> p#0 * keysC_k => {(C1x1#(p#0))_0 * (C#(keysC_k))_0, (C1x1#(p#0))_1 | (C#(keysC_k))_1, (C1x1#(p#0))_2 * (C#(keysC_k))_2} )));

    C1x1x1 := hashTable flatten listC1x1x1;
    print("#products of 3 constraints: " | toString(#C1x1x1));

    newC = merge(newC, C1x1x1, (x, y) -> x);
    if nIters == 3 then return newC;

    -- fourth factor
    keysC1x1x1 := keys C1x1x1;
    listC1x1x1x1 := for h in (0..#C-1) list (flatten apply(listC1x1x1_{0..h}, C1x1x1k -> apply(C1x1x1k, p -> p#0 * keysC_h => {(C1x1x1#(p#0))_0 * (C#(keysC_h))_0, (C1x1x1#(p#0))_1 | (C#(keysC_h))_1, (C1x1x1#(p#0))_2 * (C#(keysC_h))_2} )));

    C1x1x1x1 := hashTable flatten listC1x1x1x1;
    print("#products of 4 constraints: " | toString(#C1x1x1x1));

    newC = merge(newC, C1x1x1x1, (x, y) -> x);
    if nIters == 4 then return newC;

    -- fifth product
    keysC1x1x1x1 := keys C1x1x1x1;
    listC1x1x1x1x1 := for h in (0..#C-1) list (flatten apply(listC1x1x1x1_{0..h}, C1x1x1x1k -> apply(C1x1x1x1k, p -> p#0 * keysC_h => {(C1x1x1x1#(p#0))_0 * (C#(keysC_h))_0, (C1x1x1x1#(p#0))_1 | (C#(keysC_h))_1, (C1x1x1x1#(p#0))_2 * (C#(keysC_h))_2} )));

    C1x1x1x1x1 := hashTable flatten listC1x1x1x1x1;
    print("#products of 5 constraints: " | toString(#C1x1x1x1x1));

    newC = merge(newC, C1x1x1x1x1, (x, y) -> x);
    return newC;
);


splitStandardForm = stFormPoly -> (
    -- Isolates constant term in the standard form of a polynomial
    if stFormPoly#?(hashTable {}) then (
        mutableHash := new MutableHashTable from stFormPoly;
        remove(mutableHash, hashTable {});
        return({stFormPoly#(hashTable {}), new HashTable from mutableHash});
    );
    return({0, stFormPoly});
);


dumpListToFile = (fullList, fullFileName) -> (
    sparseMatrix := replace("[{,]", "", toString fullList);
    sparseMatrix = replace("(} |}})", "\n", sparseMatrix);
    f := fullFileName << "";
    f << sparseMatrix << endl;
    f << close;
);


getCoefficientMatrix = (polyHash, fileName) -> (
    -- INPUT: hash table polyHash whose keys are polynomials in a ring of the form QQ[x_0..x_n] (variables have to have the form x_i with underscores) and whose values are list of pairs (i,j) to identify polynomial in slack matrix, string fileName containing the prefix for name of the files.
    -- OUTPUT: list listMons of strings for linearized monomials and constant terms, list coeffMatrixList of #polylist lists representing the coefficients of each polynomial over listMons (sparse form).
    print("\n---- CONSTRUCTING COEFFICIENT MATRIX ----");
    print("Creating standard form of polynomials.");
    polyList := keys polyHash;
    elapsedTime stFormPolys := apply(polyList, p -> standardForm p);

    print("\nIsolating constant terms.");
    elapsedTime splitStFormPolys := apply(stFormPolys, sp -> splitStandardForm sp);

    print("\nCreating lists of monomials, coefficients, and constant terms.");
    elapsedTime (
        listMonsPerPoly := apply(splitStFormPolys, sp -> keys(sp_1));
        sparseMatrixEntriesList := apply(splitStFormPolys, sp -> values(sp_1));
        sparseMatrixEntries := flatten sparseMatrixEntriesList;
        constantTerms := apply(splitStFormPolys, sp -> sp_0);
    );

    print("\nCreating list of unique monomials.");
    elapsedTime (
        listMons := toList set(flatten listMonsPerPoly);
        f := fileName | "_listMons.txt" << "";
        f << apply(listMons, m->toString m) << endl;
        f << close;

        hashListMons := hashTable for j to (#listMons-1) list listMons_j => (j+1);
    );

    print("\nGenerating row and column indices for sparse matrix.");
    elapsedTime (
        sparseMatrixRows := flatten for j to (#listMonsPerPoly-1) list toList(#listMonsPerPoly_j:(j+1));
        sparseMatrixColsList := apply(listMonsPerPoly, mp -> apply(mp, m -> hashListMons#m));
        sparseMatrixCols := flatten sparseMatrixColsList;
    );
    print("\nAppending constant term entries.");
    elapsedTime (
        indicesConstantTerm := positions(constantTerms, v -> v != 0);
        if #indicesConstantTerm > 0 then (
            sparseMatrixRows = join(sparseMatrixRows, apply(indicesConstantTerm, i -> i+1));
            sparseMatrixCols = join(sparseMatrixCols, #indicesConstantTerm:(#listMons+1));
            sparseMatrixEntries = join(sparseMatrixEntries, constantTerms_indicesConstantTerm);
        ) else (
            sparseMatrixRows = append(sparseMatrixRows, 1);
            sparseMatrixCols = append(sparseMatrixCols, #listMons+1);
            sparseMatrixEntries = append(sparseMatrixEntries, 0);
            print("Warning: no constant term column, adding a dummy 0 entry in position (1, " | toString(#listMons+1) | ").");
        );
    );

    print("\nCreating sparse hash table of constraints for backtracking.");
    elapsedTime (
        sparseMatrixHash := hashTable for i to #polyList-1 list polyList_i => (sparseMatrixColsList_i | {#listMons+1}, sparseMatrixEntriesList_i | {constantTerms_i});
        sparsePolyHash := applyKeys(polyHash, k -> sparseMatrixHash#k, (x,y) -> (x,y));
    );

    constraintsFile := fileName | "_constraints.txt";
    print("Dumping sparse hash table of constraints to file " | constraintsFile);
    g := constraintsFile << "";
    g << toString sparsePolyHash << endl;
    g << close;

    print("\n-> Number of rows: " | toString(#listMonsPerPoly));
    print("-> Number of columns: " | toString(#listMons+1));
    print("-> Number of non-zero entries: " | toString(#sparseMatrixRows));

    return(sparseMatrixRows, sparseMatrixCols, sparseMatrixEntries);
);


---------------------------------------------------------------------------------------------------------
-- COMPUTE COEFFICIENT MATRIX OF SPHERES IN DATABASE

SphereDatabase = {"Altshuler_M963", "Altshuler_N10_3574", "Doolittle_11v", "Doolittle_13v1", "Doolittle_13v2", "Novik_Zheng_12v", "Novik_Zheng_14v", "Firsching_F374225", "Firsching_T2775", "Zheng_16v", "Criado_Santos_P1039", "Criado_Santos_P1963", "Criado_Santos_P2669", "Criado_Santos_P3513", "Criado_Santos_P2105"};


matrixSphereNew = method(Options => {LoadData => false, Facets => {}, Dimension => 0, ForceNewFlag => false, Prismatoid => false, Oriented => false, DumpSage => false, SmallDeterminants => false, RedundantNonsimplicialFacets => false, DumpConstraints => false, Suffix => "", Flag => {}, SelectedColumns => {}, SelectedRows => {}, IncludeRows => {}, RandomSeed => 0, SwitchToSage => false, DeepSignCheck => false})
matrixSphereNew (String, ZZ, ZZ) := ZZ => opts -> (fileName, nIter, maxDeg) -> (
    -- INPUT : 
    --      nIter = number of factors in products of constraints
    --      maxDeg = maximum degree of constraints to consider
    (F, d, fl, S1, T) := (opts.Facets, opts.Dimension, {}, {}, {});
    if opts.Prismatoid then (
        prismatoidNum := fileName;
        fileName = "Criado_Santos_P" | fileName;
        print("\n---- PRISMATOID : " | fileName | " ----\n");
    ) else print("\n---- SPHERE : " | fileName | " ----\n");

    if opts.LoadData then (
        print("Loading (F, d, fl, ringS, S1, T) from file : " | fileName | "_data.txt");
        (F, d, fl, ringS, S1, T) = value get(fileName | "_data.txt");
    ) else if F == {} then (
        if opts.Prismatoid then (
            print("Loading facet list from file : Prismatoids/" | prismatoidNum);
            F = value get("Prismatoids/" | prismatoidNum);
            d = 5;
        ) else (
            print("Loading (F, d, fl, ringS, S1, T) from function sphere");
            (F, d, fl, ringS, S1, T) = sphere(fileName);
        );
    );

    print("Setting random seed to : " | toString opts.RandomSeed);
    setRandomSeed opts.RandomSeed;

    n := #(unique flatten F);
    print("\nTotal number of vertices : " | toString n);
    print("Total number of facets : " | toString(#F));
    print("Dimension : " | toString d);
    print("Number of factors: " | toString nIter);
    print("Maximum degree of constraints: " | toString maxDeg);
    S := symbolicSlackMatrix(F, Object => "abstractPolytope");
    ringS := ring S;

    suffix = opts.Suffix;

    -- Find out which sets of vertices span the nonsimplicial facets in our choice of flag
    nonsimplicialFacets := select(F, f -> #f != d);
    print("\nNonsimplicial facets : " | toString(nonsimplicialFacets));
    numNonsimplFacets := #nonsimplicialFacets;
    print("Number of nonsimplicial facets = " | toString(numNonsimplFacets));
    indNonsimplFacets := apply(nonsimplicialFacets, f -> position(F, e -> e==f));
    print("Indices nonsimplicial facets : " | toString indNonsimplFacets);
    indSimplFacets := toList(0..#F-1) - set(indNonsimplFacets);

    fl = if opts.ForceNewFlag then {} else (if #opts.Flag > 0 then opts.Flag else fl);

    computeNewFlag := (#fl < d+1 or #findFlag(S_fl) != d+1);
    if computeNewFlag then (
        print(" -- warning: Given list of facets is not a flag. Recomputing flag:");
        -- generating new flag
        cardFlag := 0;
        numOtherIndices := d + 1 - numNonsimplFacets;
        numSampleNonsimplicial := min(d+1, numNonsimplFacets);
        numSampleSimplicial := max(0, numOtherIndices);
        count := 0;
        while cardFlag != d+1 and count < 100 do (
            fl = sort(
                (random(indNonsimplFacets))_{0..numSampleNonsimplicial - 1} | 
                (random(indSimplFacets))_{0..numSampleSimplicial - 1}
            );
            cardFlag = #findFlag(S_fl);
            count = count + 1;
        );
        if count == 100 then error("Flag not found after 100 attempts.");
        setRandomSeed opts.RandomSeed;
    );

    if (computeNewFlag or S1 === {} or T === {}) then (S1, T) = setOnesForest(S_fl);

    print("\nFlag : " | toString(fl));
    print("\nFacets in flag : " | toString(F_fl));
    print("Number of variables in ringS : " | toString(numgens ringS));

    fileNameNew := fileName | "_iter" | toString(nIter) | "_deg" | toString(maxDeg);
    if opts.Suffix != "" then fileNameNew = fileNameNew | "_" | opts.Suffix;

    print("\nDumping (F, d, fl, ringS, S1) into file: " | fileName | "_data.txt");
    fileNameData := fileName | "_data.txt" << "";
    fileNameData << "(\n" | toString F | ",\n" | toString d | ",\n" | toString fl | ",\nQQ[x_0..x_" | toString(numgens ringS) | "],\n" | toString S1 | ",\n" | toString(T) | "\n);" << endl;
    fileNameData << close;

    (Fbases, posSatHash) := getConstraints((F, d, fl, S1), fileName, Oriented => opts.Oriented, SmallDeterminants => opts.SmallDeterminants, RedundantNonsimplicialFacets => opts.RedundantNonsimplicialFacets, SwitchToSage => opts.SwitchToSage, DeepSignCheck => opts.DeepSignCheck);

    selectedRows := {};
    selectedCols := toList(0..#Fbases-1);
    if opts.SelectedRows == {} then (
        selectedRows = toList(0..n-1);
    ) else (
        selectedRows = opts.SelectedRows;
        selectedCols = sort apply(select(Fbases, f -> #(set(f) * set(toList(0..n-1) - set(selectedRows))) == 0), g -> position(Fbases, h -> h==g));
    );

    if opts.SelectedColumns != {} then selectedCols = toList(set(selectedCols) * set(opts.SelectedColumns));

    if #selectedCols < d+1 then (
        error("Number of selected columns is less than dimension!");
        return 1;
    );

    print("\nSelected vertices : " | toString(sort selectedRows));
    print("Selected facets : " | toString(sort selectedCols));
    (numSelRows, numSelCols) := (#selectedRows, #selectedCols);
    print("\nNumber of selected vertices : " | toString(numSelRows));
    print("Number of selected facets : " | toString(numSelCols));

    if opts.SwitchToSage then (
        print("Dumping Sage constraints to file.\n");
        varsS1 := unique flatten entries S1 - set{0_(ring S1), 1_(ring S1)};

        f1 := fileName | "_varSage.txt" << "";
        f1 << toString varsS1 << endl;
        f1 << close;

        f2 := fileName | "_SelRows_SelCols.txt" << "";
        f2 << toString((selectedRows, selectedCols)) << endl;
        f2 << close;
        return 1;
    );
    print("Total number of constraints before selection: " | toString(#keys(posSatHash)));

    print("\nSelecting constraints");
    if opts.IncludeRows == {} then (
        if (numSelRows, numSelCols) == (n, #Fbases) then (
            -- we switch keys and values to remove duplicate polynomials
            posSatHash = applyPairs(posSatHash, (k, v) -> if v != 1 then (v, k));
        ) else if numSelCols < #Fbases and numSelRows == n then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_1, selectedCols) and v != 1 then (v, k));
        ) else if numSelRows < n and numSelCols == #Fbases then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_0, selectedRows) and v != 1 then (v, k));
        ) else if numSelCols < #Fbases and numSelRows < n then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_0, selectedRows) and member(((k_1)_0)_1, selectedCols) and v != 1 then (v, k));
        ) else error("Number of selected rows or columns exceeds the total number of vertices or facets");
    ) else (
        if (numSelRows, numSelCols) == (n, #Fbases) then (
            -- we switch keys and values to remove duplicate polynomials
            posSatHash = applyPairs(posSatHash, (k, v) -> if isSubset(opts.IncludeRows, {((k_1)_0)_0} | Fbases_(((k_1)_0)_1)) and v != 1 then (v, k));
        ) else if numSelCols < #Fbases and numSelRows == n then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_1, selectedCols) and isSubset(opts.IncludeRows, {((k_1)_0)_0} | Fbases_(((k_1)_0)_1)) and v != 1 then (v, k));
        ) else if numSelRows < n and numSelCols == #F then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_0, selectedRows) and isSubset(opts.IncludeRows, {((k_1)_0)_0} | Fbases_(((k_1)_0)_1)) and v != 1 then (v, k));
        ) else if numSelCols < #Fbases and numSelRows < n then (
            posSatHash = applyPairs(posSatHash, (k, v) -> if member(((k_1)_0)_0, selectedRows) and isSubset(opts.IncludeRows, {((k_1)_0)_0} | Fbases_(((k_1)_0)_1)) and member(((k_1)_0)_1, selectedCols) and v != 1 then (v, k));
        ) else error("Number of selected rows or columns exceeds the total number of vertices or facets");
    );

    print("Total number of selected constraints : " | toString(#keys(posSatHash)));
    posSatHashUpToDeg := applyPairs(posSatHash, (k,v) -> if (degree(k))_0 <= maxDeg then (k, v));
    collectGarbage();

    print(toString(sort keys(posSatHashUpToDeg)));

    if #posSatHashUpToDeg == 0 then error("No constraints up to degree " | toString(maxDeg));

    ringHash := ring((keys posSatHashUpToDeg)_0);
    varsS1 = apply(unique flatten entries S1 - set{0_(ring S1), 1_(ring S1)}, y -> substitute(y, ringHash));
    
    if opts.DumpSage then (
        print("Dumping Sage constraints to file.\n");
        f3 := fileName | "_varSage.txt" << "";
        f3 << toString varsS1 << endl;
        f3 << close;

        f4 := fileNameNew | "_polySage.txt" << "";
        f4 << toString apply(pairs(posSatHashUpToDeg), e -> hashTableToPython(e)) << endl;
        f4 << close;
        return 1;
    );

    print("\n\n---- PRODUCTS OF CONSTRAINTS UP TO " | toString(nIter) | " FACTORS ----");
    print("#variables in S1 : " | toString(#varsS1));
    print("#constraints up to degree " | toString(maxDeg) | " : " | toString(#posSatHashUpToDeg));
    elapsedTime newConstraintsHash := getFullProducts(posSatHashUpToDeg, nIter);
    collectGarbage();

    newConstraintsHash = merge(newConstraintsHash, hashTable(apply(varsS1, v -> {v, {1, {(-index(v),-index(v))}, 1}})), (x, y) -> x);

    print("Total number of constraints of degree <= " | toString(maxDeg) | " and their products up to " | toString(nIter) | " factors : " | toString(#newConstraintsHash) | "\n");

    if opts.DumpConstraints == true then (
        print("Dumping constraints to file.\n");
        f5 := fileNameNew | "_polyConstraints.txt" << "";
        f5 << toString newConstraintsHash << endl;
        f5 << close;
    );

    (sparseMatrixRows, sparseMatrixCols, sparseMatrixEntries) := getCoefficientMatrix(newConstraintsHash, fileNameNew);
    fullFileName := fileNameNew | "_matrix.txt";
    print("\nDumping sparse matrix to file " | fullFileName);
    elapsedTime dumpListToFile({sparseMatrixRows, sparseMatrixCols, sparseMatrixEntries}, fullFileName);

    return 1;
);


hashTableToPython = e -> (
    return toString(e_0) | ":[[" | toString ((e_1)_1)_0 | "], " | toString (e_1)_2 | "]!";
);


---------------------------------------------------------------------------------------------------------
-- SPHERES

sphere = method()
sphere String := Sequence => name -> (
    -- INPUT:   fileName = string identifying the sphere
    -- OUTPUT:  F = list of facets as list of vertex labels
    --          d = integer, dimension of sphere
    --          fl = list, containing column indices of a flag
    --          ringS = polynomial ring of the full slack matrix
    --          S1 = reduced dehomogenized slack matrix corresponding to flag fl
    if name == "Altshuler_M963" then (
        return (
            {{0, 1, 2, 4}, {0, 1, 7, 8}, {2, 5, 7, 8}, {0, 2, 5, 7}, {2, 3, 6, 7}, {2, 4, 5, 8}, {4, 5, 6, 8}, {1, 2, 3, 4}, {1, 5, 7, 8}, {0, 1, 2, 6}, {0, 2, 4, 5}, {1, 2, 3, 6}, {2, 3, 4, 8}, {3, 4, 5, 6}, {0, 1, 4, 5}, {1, 3, 4, 5}, {3, 4, 6, 8}, {0, 1, 5, 7}, {0, 2, 6, 7}, {1, 3, 5, 6}, {2, 3, 7, 8}, {3, 6, 7, 8}, {0, 1, 6, 8}, {0, 6, 7, 8}, {1, 4, 6, 8}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_124],
            matrix {{0, 0, 1, 0, x_1}, {0, 0, x_15, 1, x_17}, {0, 1, 0, 0, 0}, {x_41, 1, x_43, x_44, 0}, {0, 1, x_57, x_58, 1}, {x_70, 1, 0, 0, x_72}, {1, 1, 1, 1, 0}, {1, 0, 0, 0, 0}, {1, 0, 0, x_113, x_114}},
            graph(QQ[y_0..y_13], {{y_6, y_9}, {y_7, y_9}, {y_8, y_9}, {y_2, y_10}, {y_3, y_10}, {y_4, y_10}, {y_5, y_10}, {y_6, y_10}, {y_0, y_11}, {y_6, y_11}, {y_1, y_12}, {y_6, y_12}, {y_4, y_13}})
        );
    ) else if name == "Altshuler_N10_3574" then (
        return (
            {{2, 3, 7, 8}, {2, 4, 7, 8}, {1, 2, 6, 7}, {2, 3, 5, 8}, {3, 5, 8, 9}, {0, 2, 5, 6}, {2, 5, 6, 8}, {0, 2, 5, 9}, {3, 4, 8, 9}, {0, 3, 4, 8}, {0, 2, 6, 9}, {0, 3, 7, 8}, {1, 2, 6, 8}, {3, 6, 7, 9}, {0, 1, 5, 7}, {1, 4, 8, 9}, {0, 4, 5, 9}, {0, 4, 5, 7}, {1, 4, 5, 9}, {1, 2, 4, 7}, {2, 3, 5, 9}, {1, 4, 5, 7}, {1, 3, 6, 7}, {0, 1, 3, 7}, {0, 3, 4, 6}, {0, 1, 3, 6}, {1, 5, 6, 8}, {2, 6, 7, 9}, {0, 4, 7, 8}, {2, 3, 7, 9}, {1, 5, 8, 9}, {0, 4, 6, 9}, {3, 4, 6, 9}, {1, 2, 4, 8}, {0, 1, 5, 6}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_209],
            matrix {{x_0, x_1, x_2, 1, x_4}, {x_21, x_22, 0, 1, x_24}, {0, 0, 0, 0, 1}, {0, 1, x_64, 0, 0}, {x_84, 0, x_85, 1, x_87}, {x_105, 1, x_107, 0, 0}, {1, 1, 0, 1, 1}, {0, 0, 0, 1, x_148}, {0, 0, 1, 0, 0}, {x_189, x_190, 1, 1, 0}},
            graph(QQ[y_0..y_14], {{y_6, y_10}, {y_3, y_11}, {y_5, y_11}, {y_6, y_11}, {y_8, y_12}, {y_9, y_12}, {y_0, y_13}, {y_1, y_13}, {y_4, y_13}, {y_6, y_13}, {y_7, y_13}, {y_9, y_13}, {y_2, y_14}, {y_6, y_14}})
        );
    ) else if name == "Doolittle_11v" then (
        return (
            {{4, 5, 6, 7}, {0, 2, 6, 10}, {6, 7, 8, 9}, {5, 6, 7, 8}, {3, 7, 8, 10}, {0, 2, 3, 6}, {0, 3, 7, 8}, {0, 5, 7, 8}, {2, 6, 7, 10}, {3, 4, 5, 6}, {3, 5, 6, 9}, {2, 3, 6, 9}, {0, 1, 2, 10}, {0, 4, 5, 8}, {1, 4, 8, 9}, {3, 4, 7, 10}, {2, 3, 4, 5}, {0, 4, 5, 7}, {4, 6, 7, 10}, {0, 1, 2, 3}, {2, 7, 9, 10}, {1, 2, 3, 4}, {1, 5, 6, 9}, {2, 5, 9, 10}, {2, 3, 5, 9}, {0, 4, 8, 9}, {0, 1, 9, 10}, {1, 6, 8, 9}, {1, 5, 9, 10}, {0, 3, 8, 10}, {1, 5, 6, 8}, {0, 8, 9, 10}, {2, 6, 7, 9}, {7, 8, 9, 10}, {2, 4, 5, 8}, {0, 3, 6, 10}, {1, 2, 5, 10}, {0, 1, 3, 7}, {1, 2, 5, 8}, {0, 1, 4, 9}, {1, 3, 4, 7}, {0, 1, 4, 7}, {3, 4, 6, 10}, {1, 2, 4, 8}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_307],
            matrix {{x_0, 0, x_1, 1, x_3}, {x_28, x_29, x_30, 1, x_32}, {x_56, 0, x_57, 1, 1}, {1, 1, 1, 1, 0}, {0, x_112, x_113, 1, x_115}, {0, 1, x_141, 0, x_142}, {0, 0, 0, 0, 1}, {0, 1, 0, 0, 0}, {1, x_225, 0, 0, 0}, {x_252, x_253, 0, 1, x_255}, {x_280, 0, x_281, 1, 0}},
            graph(QQ[y_0..y_15], {{y_3, y_11}, {y_8, y_11}, {y_3, y_12}, {y_5, y_12}, {y_7, y_12}, {y_3, y_13}, {y_0, y_14}, {y_1, y_14}, {y_2, y_14}, {y_3, y_14}, {y_4, y_14}, {y_9, y_14}, {y_10, y_14}, {y_2, y_15}, {y_6, y_15}})
        );
    ) else if name == "Doolittle_13v1" then (
        return (
            {{2, 3, 6, 10}, {2, 6, 10, 12}, {2, 3, 9, 10}, {0, 1, 6, 7}, {2, 6, 11, 12}, {1, 2, 5, 9}, {0, 1, 4, 6}, {0, 5, 6, 9}, {1, 2, 5, 7}, {1, 4, 6, 9}, {2, 5, 7, 10}, {0, 6, 7, 12}, {0, 2, 5, 10}, {0, 4, 6, 9}, {0, 5, 6, 12}, {1, 5, 9, 11}, {2, 6, 8, 11}, {5, 6, 11, 12}, {2, 3, 8, 9}, {2, 4, 7, 12}, {3, 6, 8, 11}, {5, 6, 9, 11}, {0, 3, 7, 11}, {3, 4, 7, 9}, {3, 7, 9, 12}, {1, 2, 8, 9}, {0, 3, 8, 11}, {0, 1, 7, 8}, {1, 3, 6, 11}, {4, 5, 8, 10}, {0, 2, 5, 9}, {3, 5, 8, 12}, {3, 8, 9, 12}, {3, 4, 7, 11}, {2, 4, 11, 12}, {2, 7, 8, 11}, {1, 6, 7, 10}, {1, 8, 9, 12}, {0, 3, 5, 12}, {1, 3, 10, 11}, {0, 4, 9, 10}, {1, 4, 9, 12}, {0, 1, 4, 8}, {1, 4, 8, 12}, {4, 7, 9, 12}, {0, 5, 8, 10}, {3, 4, 10, 11}, {3, 4, 9, 10}, {4, 5, 8, 12}, {6, 7, 10, 12}, {1, 5, 10, 11}, {4, 5, 11, 12}, {0, 2, 9, 10}, {2, 4, 7, 11}, {4, 5, 10, 11}, {1, 6, 9, 11}, {1, 5, 7, 10}, {1, 2, 7, 8}, {0, 3, 5, 8}, {2, 3, 6, 8}, {0, 4, 8, 10}, {2, 7, 10, 12}, {0, 3, 7, 12}, {1, 3, 6, 10}, {0, 7, 8, 11}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_584],
            matrix {{x_0, x_1, x_2, 0, 1}, {x_45, x_46, x_47, 0, 1}, {0, 0, 0, 1, 0}, {0, x_135, 0, x_136, 1}, {x_180, x_181, x_182, x_183, 1}, {1, 1, 1, 1, 1}, {0, 0, 1, 0, 0}, {x_315, x_316, x_317, 0, 1}, {x_360, x_361, x_362, x_363, 1}, {x_405, x_406, 0, x_407, 1}, {0, 0, 0, x_450, 1}, {1, x_496, x_497, x_498, 0}, {1, 0, x_541, x_542, 0}},
            graph(QQ[y_0..y_17], {{y_5, y_13}, {y_11, y_13}, {y_12, y_13}, {y_5, y_14}, {y_5, y_15}, {y_6, y_15}, {y_2, y_16}, {y_5, y_16}, {y_0, y_17}, {y_1, y_17}, {y_3, y_17}, {y_4, y_17}, {y_5, y_17}, {y_7, y_17}, {y_8, y_17}, {y_9, y_17}, {y_10, y_17}})
        );
    ) else if name == "Doolittle_13v2" then (
        return (
            {{3, 4, 7, 12}, {4, 5, 7, 12}, {0, 1, 3, 8}, {4, 5, 8, 12}, {3, 9, 10, 12}, {5, 7, 11, 12}, {2, 3, 6, 10}, {1, 2, 5, 9}, {2, 6, 11, 12}, {0, 6, 7, 9}, {1, 6, 7, 9}, {2, 7, 11, 12}, {1, 3, 7, 8}, {1, 6, 11, 12}, {0, 2, 7, 12}, {0, 5, 6, 9}, {1, 5, 6, 12}, {3, 5, 9, 10}, {1, 7, 8, 10}, {2, 8, 9, 11}, {0, 6, 8, 12}, {3, 4, 10, 12}, {3, 8, 9, 12}, {1, 6, 10, 11}, {4, 9, 10, 12}, {3, 4, 7, 11}, {4, 5, 7, 11}, {2, 7, 8, 10}, {4, 8, 9, 12}, {0, 5, 9, 10}, {2, 7, 8, 11}, {1, 6, 7, 10}, {0, 4, 10, 11}, {3, 4, 6, 10}, {1, 2, 4, 9}, {1, 2, 4, 8}, {3, 7, 8, 11}, {0, 4, 9, 10}, {0, 1, 4, 8}, {2, 6, 7, 10}, {2, 3, 9, 11}, {0, 2, 6, 7}, {3, 4, 6, 11}, {2, 4, 8, 9}, {0, 5, 10, 11}, {1, 5, 10, 11}, {1, 5, 6, 9}, {2, 3, 5, 9}, {0, 1, 3, 7}, {0, 3, 8, 12}, {4, 6, 10, 11}, {0, 1, 7, 9}, {1, 5, 11, 12}, {1, 2, 5, 10}, {3, 8, 9, 11}, {0, 4, 5, 8}, {0, 5, 6, 8}, {1, 2, 8, 10}, {0, 2, 6, 12}, {0, 1, 4, 9}, {0, 4, 5, 11}, {5, 6, 8, 12}, {0, 3, 7, 12}, {2, 3, 5, 10}, {2, 3, 6, 11}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_584],
            matrix {{x_0, x_1, 0, x_2, 1}, {x_45, x_46, 0, x_47, 1}, {x_90, x_91, x_92, x_93, 1}, {0, x_135, 0, 1, 0}, {0, 0, x_180, 0, 1}, {x_225, 0, x_226, 0, 1}, {1, 1, 1, 1, 1}, {0, 0, x_315, x_316, 1}, {x_360, x_361, 0, 0, 1}, {1, x_406, x_407, x_408, 0}, {1, x_451, x_452, x_453, 0}, {x_495, x_496, x_497, x_498, 1}, {0, 0, 1, 0, 0}},
            graph(QQ[y_0..y_17], {{y_0, y_17}, {y_1, y_17}, {y_2, y_17}, {y_3, y_16}, {y_4, y_17}, {y_5, y_17}, {y_6, y_13}, {y_6, y_14}, {y_6, y_15}, {y_6, y_16}, {y_6, y_17}, {y_7, y_17}, {y_8, y_17}, {y_9, y_13}, {y_10, y_13}, {y_11, y_17}, {y_12, y_15}})
        );
    ) else if name == "Novik_Zheng_12v" then (
        return (
            {{1, 7, 9, 11}, {1, 2, 6, 8}, {1, 3, 7, 11}, {1, 2, 6, 7}, {0, 5, 7, 8}, {1, 2, 3, 5}, {2, 3, 4, 6}, {2, 4, 6, 11}, {1, 2, 4, 5}, {2, 3, 4, 5}, {3, 4, 5, 11}, {1, 2, 3, 7}, {0, 1, 2, 4}, {2, 4, 10, 11}, {0, 2, 4, 10}, {7, 9, 10, 11}, {0, 1, 7, 9}, {0, 1, 3, 5}, {3, 5, 10, 11}, {0, 3, 5, 9}, {6, 8, 10, 11}, {0, 1, 6, 8}, {2, 6, 8, 11}, {3, 5, 9, 10}, {6, 8, 9, 10}, {4, 8, 9, 10}, {0, 4, 8, 10}, {5, 7, 8, 9}, {0, 5, 7, 9}, {0, 1, 3, 9}, {0, 1, 2, 8}, {0, 2, 8, 10}, {2, 8, 10, 11}, {3, 9, 10, 11}, {1, 3, 9, 11}, {0, 1, 4, 5}, {4, 5, 10, 11}, {6, 7, 10, 11}, {0, 1, 6, 7}, {4, 5, 9, 10}, {6, 7, 9, 10}, {4, 5, 8, 9}, {6, 7, 8, 9}, {2, 3, 6, 7}, {0, 4, 5, 8}, {0, 6, 7, 8}, {3, 6, 7, 11}, {3, 4, 6, 11}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_383],
            matrix {{x_0, x_1, x_2, 1, 0}, {0, 0, 0, 0, 1}, {x_64, 0, x_65, 0, 1}, {x_96, x_97, 0, x_98, 1}, {1, 1, 1, 1, 1}, {x_160, x_161, x_162, 1, 0}, {x_192, 0, x_193, 0, 1}, {0, 1, 0, 0, 0}, {x_256, 0, x_257, 1, 0}, {0, x_288, x_289, x_290, 1}, {x_320, x_321, x_322, x_323, 1}, {0, x_352, 0, x_353, 1}},
            graph(QQ[y_0..y_16], {{y_4, y_12}, {y_4, y_13}, {y_7, y_13}, {y_4, y_14}, {y_0, y_15}, {y_4, y_15}, {y_5, y_15}, {y_8, y_15}, {y_1, y_16}, {y_2, y_16}, {y_3, y_16}, {y_4, y_16}, {y_6, y_16}, {y_9, y_16}, {y_10, y_16}, {y_11, y_16}})
        );
    ) else if name == "Firsching_F374225" then (
        return (
            {{0, 2, 3, 6}, {0, 3, 6, 7}, {1, 4, 10, 11}, {2, 3, 5, 6}, {2, 3, 5, 10}, {0, 1, 4, 8}, {0, 1, 4, 10}, {0, 1, 8, 9}, {0, 1, 9, 10}, {0, 2, 3, 11}, {0, 2, 4, 9}, {0, 2, 4, 11}, {0, 2, 6, 7}, {0, 2, 7, 9}, {0, 3, 7, 9}, {0, 3, 9, 11}, {0, 4, 5, 8}, {0, 4, 5, 9}, {0, 4, 10, 11}, {0, 5, 8, 9}, {0, 9, 10, 11}, {1, 2, 3, 4}, {1, 2, 3, 8}, {1, 2, 4, 5}, {1, 2, 5, 8}, {1, 3, 4, 11}, {1, 3, 8, 11}, {1, 4, 5, 8}, {1, 6, 7, 9}, {1, 6, 7, 10}, {1, 6, 8, 9}, {1, 6, 8, 10}, {1, 7, 9, 10}, {1, 8, 10, 11}, {2, 3, 4, 11}, {2, 3, 8, 10}, {2, 4, 5, 6}, {2, 4, 6, 7}, {2, 4, 7, 9}, {2, 5, 8, 10}, {3, 5, 6, 11}, {3, 5, 10, 11}, {3, 6, 7, 11}, {3, 7, 9, 11}, {3, 8, 10, 11}, {4, 5, 6, 7}, {4, 5, 7, 9}, {5, 6, 7, 8}, {5, 6, 8, 10}, {5, 6, 10, 11}, {5, 7, 8, 9}, {6, 7, 8, 9}, {6, 7, 10, 11}, {7, 9, 10, 11}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_431],
            matrix {{0, 0, 1, x_1, x_2}, {x_36, x_37, 0, x_38, 1}, {0, x_72, 1, 0, 0}, {0, 0, 1, 0, 0}, {x_144, x_145, 0, x_146, 1}, {1, 1, 1, 0, 0}, {0, 0, 1, 0, 1}, {x_252, 0, 1, x_254, x_255}, {x_288, x_289, 1, 1, x_292}, {x_324, x_325, 1, x_327, x_328}, {1, x_361, 0, x_362, 0}, {x_396, x_397, 0, x_398, 1}},
            graph(QQ[y_0..y_16], {{y_5, y_12}, {y_10, y_12}, {y_5, y_13}, {y_0, y_14}, {y_2, y_14}, {y_3, y_14}, {y_5, y_14}, {y_6, y_14}, {y_7, y_14}, {y_8, y_14}, {y_9, y_14}, {y_8, y_15}, {y_1, y_16}, {y_4, y_16}, {y_6, y_16}, {y_11, y_16}})
        ); 
    ) else if name == "Firsching_T2775" then (
        return (
            {{3, 4, 6, 7}, {5, 7, 8, 13}, {5, 8, 9, 13}, {5, 9, 12, 13}, {8, 9, 10, 13}, {0, 1, 2, 3}, {0, 1, 2, 4}, {0, 1, 3, 5}, {0, 1, 4, 6}, {0, 1, 5, 6}, {0, 2, 3, 4}, {0, 3, 4, 7}, {0, 3, 5, 7}, {0, 4, 6, 7}, {0, 5, 6, 8}, {0, 5, 7, 8}, {0, 6, 7, 8}, {1, 2, 3, 9}, {1, 2, 4, 10}, {1, 2, 9, 10}, {1, 3, 5, 11}, {1, 3, 9, 11}, {1, 4, 6, 12}, {1, 4, 10, 12}, {1, 5, 6, 12}, {1, 5, 11, 12}, {1, 9, 10, 11}, {1, 10, 11, 12}, {2, 3, 4, 12}, {2, 3, 9, 12}, {2, 4, 10, 12}, {2, 9, 10, 13}, {2, 9, 12, 13}, {2, 10, 12, 13}, {3, 4, 6, 12}, {3, 5, 7, 11}, {3, 6, 7, 11}, {3, 6, 9, 11}, {3, 6, 9, 12}, {5, 6, 8, 9}, {5, 6, 9, 12}, {5, 7, 11, 13}, {5, 11, 12, 13}, {6, 7, 8, 11}, {6, 8, 9, 11}, {7, 8, 11, 13}, {8, 9, 10, 11}, {8, 10, 11, 13}, {10, 11, 12, 13}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_489],
            matrix {{1, x_1, x_2, x_3, x_4}, {1, x_38, x_39, x_40, x_41}, {1, 1, 1, 1, x_74}, {0, x_107, x_108, x_109, 1}, {0, x_140, x_141, x_142, 1}, {1, 0, 0, 0, 1}, {0, x_210, x_211, x_212, 1}, {0, 0, x_243, x_244, 1}, {1, 0, 0, x_281, 0}, {1, x_318, 0, 0, 0}, {1, x_351, x_352, x_353, 0}, {1, x_388, x_389, x_390, x_391}, {1, x_421, x_422, 0, x_423}, {1, 0, 0, 0, 0}},
            graph(QQ[y_0..y_18], {{y_0, y_14}, {y_1, y_14}, {y_2, y_14}, {y_5, y_14}, {y_8, y_14}, {y_9, y_14}, {y_10, y_14}, {y_11, y_14}, {y_12, y_14}, {y_13, y_14}, {y_2, y_15}, {y_2, y_16}, {y_2, y_17}, {y_3, y_18}, {y_4, y_18}, {y_5, y_18}, {y_6, y_18}, {y_7, y_18}})
        );
    ) else if name == "Zheng_16v" then (
        return (
            {{0, 4, 11, 15}, {3, 6, 10, 14}, {0, 4, 8, 13}, {0, 5, 9, 15}, {0, 4, 11, 13}, {1, 4, 10, 15}, {0, 5, 11, 14}, {1, 5, 9, 12}, {3, 5, 8, 13}, {3, 5, 9, 13}, {1, 7, 9, 12}, {2, 6, 8, 13}, {1, 5, 8, 14}, {0, 5, 9, 12}, {2, 5, 8, 15}, {1, 6, 11, 15}, {2, 4, 8, 14}, {0, 6, 8, 13}, {0, 6, 8, 12}, {0, 4, 8, 12}, {3, 4, 10, 14}, {3, 6, 11, 15}, {0, 4, 9, 15}, {2, 7, 8, 13}, {3, 6, 10, 15}, {3, 5, 8, 15}, {1, 7, 11, 13}, {3, 7, 9, 12}, {1, 6, 10, 12}, {2, 7, 10, 14}, {1, 5, 10, 12}, {1, 4, 11, 13}, {1, 5, 10, 14}, {2, 6, 10, 13}, {3, 7, 8, 13}, {1, 6, 10, 15}, {1, 4, 8, 13}, {1, 4, 10, 14}, {2, 5, 8, 14}, {2, 7, 8, 15}, {0, 5, 10, 12}, {0, 5, 10, 14}, {3, 4, 9, 15}, {1, 5, 9, 13}, {3, 7, 11, 12}, {1, 7, 11, 12}, {2, 5, 11, 14}, {0, 7, 10, 14}, {3, 7, 8, 15}, {2, 4, 9, 12}, {2, 6, 10, 14}, {3, 6, 9, 12}, {0, 7, 11, 13}, {2, 4, 8, 12}, {1, 5, 8, 13}, {1, 4, 11, 15}, {0, 7, 11, 14}, {2, 6, 9, 14}, {3, 5, 9, 15}, {3, 6, 9, 14}, {3, 7, 11, 15}, {0, 7, 10, 13}, {0, 5, 11, 15}, {1, 7, 9, 13}, {2, 6, 8, 12}, {2, 5, 11, 15}, {0, 4, 9, 12}, {2, 7, 11, 15}, {1, 4, 8, 14}, {3, 4, 9, 14}, {2, 7, 10, 13}, {0, 6, 10, 13}, {2, 6, 9, 12}, {0, 6, 10, 12}, {3, 6, 11, 12}, {1, 6, 11, 12}, {2, 7, 11, 14}, {3, 7, 9, 13}, {2, 4, 9, 14}, {3, 4, 10, 15}},
            4,
            {0, 1, 2, 3, 4},
            QQ[x_0..x_959],
            matrix {{0, 1, 0, 0, 0}, {1, x_61, x_62, x_63, x_64}, {1, x_121, x_122, x_123, x_124}, {1, 0, x_181, x_182, x_183}, {0, x_240, 0, 1, 0}, {1, x_301, x_302, 0, x_303}, {1, 0, 1, x_362, 1}, {1, x_421, x_422, x_423, x_424}, {1, x_481, 0, x_482, x_483}, {1, x_541, x_542, 0, x_543}, {1, 0, x_601, x_602, x_603}, {0, x_660, x_661, 1, 0}, {1, x_721, x_722, x_723, x_724}, {1, 1, 0, 1, 0}, {1, 0, x_841, x_842, x_843}, {0, 1, x_901, 0, x_902}},
            graph(QQ[y_0..y_20], {{y_1, y_16}, {y_2, y_16}, {y_3, y_16}, {y_5, y_16}, {y_6, y_16}, {y_7, y_16}, {y_8, y_16}, {y_9, y_16}, {y_10, y_16}, {y_12, y_16}, {y_13, y_16}, {y_14, y_16}, {y_0, y_17}, {y_13, y_17}, {y_15, y_17}, {y_6, y_18}, {y_4, y_19}, {y_11, y_19}, {y_13, y_19}, {y_6, y_20}})
        );
    ) else if name == "Criado_Santos_P1039" then (
        return (
            {{0, 1, 2, 3, 4, 5, 6}, {7, 8, 9, 10, 11, 12, 13}, {1, 4, 7, 8, 12}, {1, 3, 7, 8, 12}, {1, 4, 7, 8, 13}, {1, 2, 4, 7, 11}, {0, 2, 5, 6, 13}, {1, 2, 6, 9, 13}, {1, 2, 3, 7, 11}, {0, 2, 5, 7, 12}, {2, 6, 7, 8, 12}, {2, 5, 7, 8, 12}, {5, 7, 9, 10, 11}, {0, 2, 4, 5, 12}, {0, 1, 5, 8, 13}, {1, 2, 4, 7, 12}, {0, 2, 5, 8, 13}, {2, 6, 7, 9, 11}, {2, 5, 7, 8, 11}, {5, 7, 8, 10, 11}, {1, 2, 5, 6, 13}, {0, 1, 5, 8, 12}, {1, 2, 3, 7, 12}, {0, 2, 4, 7, 12}, {1, 3, 7, 9, 10}, {2, 6, 7, 8, 11}, {0, 1, 8, 9, 13}, {3, 8, 9, 12, 13}, {1, 2, 4, 5, 12}, {0, 1, 4, 7, 11}, {1, 2, 3, 8, 12}, {0, 2, 4, 7, 11}, {1, 3, 7, 9, 13}, {0, 5, 7, 8, 12}, {0, 6, 8, 9, 13}, {3, 7, 9, 12, 13}, {0, 2, 3, 4, 11}, {0, 1, 3, 7, 11}, {1, 2, 3, 8, 13}, {0, 2, 5, 7, 11}, {1, 3, 7, 8, 13}, {0, 3, 7, 9, 11}, {2, 6, 8, 9, 13}, {3, 7, 8, 12, 13}, {0, 1, 2, 3, 10}, {0, 1, 3, 7, 10}, {1, 2, 3, 9, 10}, {0, 2, 5, 8, 11}, {0, 6, 8, 9, 11}, {0, 5, 7, 9, 11}, {2, 3, 8, 9, 13}, {0, 8, 9, 10, 11}, {1, 2, 3, 4, 11}, {0, 1, 6, 9, 10}, {1, 2, 3, 9, 13}, {1, 2, 5, 8, 13}, {0, 4, 7, 8, 10}, {0, 3, 7, 9, 10}, {2, 3, 8, 9, 12}, {6, 8, 9, 11, 12}, {0, 1, 2, 6, 10}, {0, 1, 6, 9, 13}, {0, 2, 6, 8, 13}, {1, 2, 5, 8, 12}, {0, 4, 8, 9, 10}, {0, 5, 7, 9, 10}, {2, 3, 7, 9, 12}, {6, 7, 9, 11, 12}, {0, 1, 5, 6, 13}, {0, 1, 4, 8, 12}, {0, 2, 6, 9, 10}, {0, 2, 3, 9, 11}, {1, 4, 8, 9, 13}, {0, 5, 9, 10, 11}, {2, 3, 7, 9, 11}, {6, 7, 8, 11, 12}, {0, 1, 4, 5, 12}, {0, 1, 4, 7, 10}, {0, 2, 6, 9, 11}, {0, 2, 3, 9, 10}, {0, 5, 8, 10, 11}, {2, 6, 8, 9, 12}, {4, 7, 8, 9, 13}, {0, 1, 3, 4, 11}, {0, 1, 4, 9, 10}, {0, 2, 6, 8, 11}, {1, 4, 7, 9, 13}, {0, 5, 7, 8, 10}, {2, 6, 7, 9, 12}, {4, 7, 8, 9, 10}, {0, 1, 4, 8, 9}, {1, 2, 6, 9, 10}, {1, 4, 7, 9, 10}, {0, 4, 7, 8, 12}},
            5,
            {0, 1, 2, 3, 4, 5},
            QQ[x_0..x_841],
            matrix {{0, x_0, x_1, x_2, 1, x_4}, {0, 1, 0, 0, 0, 0}, {0, 1, 1, 1, 1, 0}, {0, x_148, x_149, 0, 1, x_151}, {0, x_214, 0, 1, 0, 0}, {0, x_282, x_283, x_284, 1, x_286}, {0, x_351, x_352, x_353, 1, x_355}, {1, 0, 0, 0, 0, 0}, {1, 0, 0, 0, 0, x_471}, {x_521, 0, x_522, x_523, 1, x_525}, {1, 0, x_573, x_574, 1, 1}, {x_642, 0, x_643, x_644, 1, 0}, {x_707, 0, 0, 0, 1, x_709}, {x_773, 0, 1, x_775, 0, x_776}},
            graph(QQ[y_0..y_19], {{y_7, y_14}, {y_8, y_14}, {y_10, y_14}, {y_1, y_15}, {y_2, y_15}, {y_2, y_16}, {y_13, y_16}, {y_2, y_17}, {y_4, y_17}, {y_0, y_18}, {y_2, y_18}, {y_3, y_18}, {y_5, y_18}, {y_6, y_18}, {y_9, y_18}, {y_10, y_18}, {y_11, y_18}, {y_12, y_18}, {y_10, y_19}})
        ); 
    ) else if name == "Criado_Santos_P1963" then (
        return (
            {{0, 1, 2, 3, 4, 5, 6}, {7, 8, 9, 10, 11, 12, 13}, {0, 1, 2, 6, 10}, {0, 1, 4, 7, 11}, {0, 2, 3, 8, 10}, {0, 1, 2, 3, 10}, {1, 2, 6, 9, 13}, {1, 4, 7, 8, 11}, {2, 4, 7, 9, 10}, {2, 5, 8, 9, 10}, {5, 7, 8, 10, 11}, {0, 2, 4, 7, 11}, {1, 2, 3, 8, 10}, {1, 2, 5, 9, 13}, {2, 4, 7, 8, 11}, {1, 2, 7, 9, 12}, {2, 5, 8, 9, 11}, {6, 7, 8, 11, 12}, {0, 1, 3, 4, 11}, {0, 1, 3, 7, 11}, {0, 2, 3, 8, 11}, {1, 2, 5, 9, 12}, {2, 4, 7, 8, 10}, {2, 5, 7, 9, 12}, {2, 6, 8, 9, 11}, {3, 7, 8, 12, 13}, {0, 2, 3, 4, 11}, {0, 1, 4, 7, 12}, {0, 2, 6, 8, 11}, {0, 1, 5, 9, 12}, {2, 5, 7, 8, 11}, {2, 5, 7, 9, 10}, {1, 3, 7, 9, 13}, {5, 7, 9, 10, 11}, {1, 2, 3, 4, 11}, {0, 2, 4, 7, 12}, {1, 2, 3, 8, 11}, {0, 1, 3, 9, 12}, {2, 5, 7, 8, 10}, {0, 5, 7, 9, 12}, {1, 4, 7, 9, 13}, {6, 7, 9, 11, 12}, {0, 1, 4, 5, 12}, {0, 2, 5, 7, 12}, {1, 2, 4, 8, 11}, {0, 1, 5, 9, 13}, {1, 3, 7, 8, 11}, {0, 5, 7, 9, 11}, {1, 6, 8, 9, 13}, {4, 7, 8, 9, 10}, {0, 2, 4, 5, 12}, {0, 2, 5, 7, 11}, {1, 2, 4, 8, 10}, {0, 2, 5, 9, 13}, {0, 3, 7, 8, 11}, {0, 6, 7, 9, 11}, {0, 6, 8, 9, 13}, {5, 8, 9, 10, 11}, {1, 2, 4, 5, 12}, {1, 2, 4, 7, 12}, {0, 1, 6, 8, 13}, {0, 2, 5, 9, 11}, {0, 6, 7, 8, 11}, {0, 6, 7, 9, 12}, {0, 6, 8, 9, 12}, {6, 8, 9, 11, 12}, {0, 1, 5, 6, 13}, {0, 1, 3, 7, 12}, {0, 1, 3, 8, 13}, {0, 2, 6, 9, 13}, {0, 3, 7, 8, 12}, {1, 3, 7, 9, 12}, {0, 3, 8, 9, 12}, {3, 7, 9, 12, 13}, {0, 2, 5, 6, 13}, {0, 1, 6, 8, 10}, {1, 2, 6, 9, 10}, {0, 2, 6, 9, 11}, {0, 6, 7, 8, 12}, {1, 6, 8, 9, 10}, {1, 4, 8, 9, 13}, {4, 7, 8, 9, 13}, {1, 2, 5, 6, 13}, {0, 2, 6, 8, 10}, {1, 2, 4, 9, 10}, {0, 1, 3, 9, 13}, {1, 3, 7, 8, 13}, {1, 4, 8, 9, 10}, {0, 3, 8, 9, 13}, {3, 8, 9, 12, 13}, {0, 1, 3, 8, 10}, {1, 2, 4, 7, 9}, {1, 4, 7, 8, 13}, {2, 6, 8, 9, 10}},
            5,
            {0, 1, 2, 3, 4, 5},
            QQ[x_0..x_841],
            matrix {{0, 1, 0, 0, 0, 0}, {0, x_49, 0, 0, 1, 0}, {0, 1, 0, x_100, 0, 0}, {0, 1, x_150, x_151, 0, 0}, {0, x_217, x_218, 0, x_219, 1}, {0, x_285, x_286, x_287, x_288, 1}, {0, 1, 0, 1, 1, 1}, {x_421, 0, x_422, 0, x_423, 1}, {x_473, 0, x_474, x_475, 0, 1}, {x_522, 0, x_523, x_524, x_525, 1}, {x_570, 0, 0, 1, 0, 0}, {1, 0, 1, 0, x_643, 1}, {x_705, 0, x_706, x_707, x_708, 1}, {x_772, 0, x_773, x_774, x_775, 1}},
            graph(QQ[y_0..y_19], {{y_11, y_14}, {y_0, y_15}, {y_2, y_15}, {y_3, y_15}, {y_6, y_15}, {y_11, y_16}, {y_6, y_17}, {y_10, y_17}, {y_1, y_18}, {y_6, y_18}, {y_4, y_19}, {y_5, y_19}, {y_6, y_19}, {y_7, y_19}, {y_8, y_19}, {y_9, y_19}, {y_11, y_19}, {y_12, y_19}, {y_13, y_19}})
        );
    ) else if name == "Criado_Santos_P2669" then (
        return (
            {{0, 1, 2, 3, 4, 5, 6}, {7, 8, 9, 10, 11, 12, 13}, {1, 2, 6, 9, 10}, {0, 1, 2, 6, 10}, {1, 2, 3, 9, 10}, {0, 2, 4, 5, 12}, {0, 1, 5, 6, 13}, {0, 2, 3, 7, 10}, {1, 2, 6, 9, 13}, {0, 2, 4, 7, 12}, {2, 4, 7, 8, 13}, {0, 6, 8, 9, 13}, {0, 4, 7, 8, 12}, {5, 7, 8, 10, 11}, {0, 2, 5, 6, 13}, {0, 1, 3, 7, 10}, {0, 1, 5, 9, 13}, {0, 2, 6, 7, 12}, {2, 3, 7, 8, 13}, {2, 6, 8, 9, 13}, {0, 4, 7, 8, 11}, {5, 7, 9, 10, 11}, {1, 2, 5, 6, 13}, {0, 2, 6, 7, 10}, {0, 1, 6, 9, 10}, {0, 2, 6, 8, 12}, {1, 3, 7, 9, 13}, {2, 3, 8, 9, 13}, {0, 6, 7, 8, 11}, {5, 8, 9, 10, 11}, {0, 1, 2, 3, 10}, {0, 1, 3, 7, 11}, {0, 2, 5, 8, 12}, {1, 4, 7, 9, 13}, {0, 5, 8, 9, 13}, {1, 4, 7, 8, 11}, {4, 7, 8, 9, 13}, {0, 3, 4, 7, 11}, {1, 2, 5, 8, 12}, {1, 3, 7, 9, 10}, {1, 5, 8, 9, 13}, {1, 4, 7, 8, 10}, {4, 7, 8, 9, 10}, {0, 1, 3, 4, 11}, {2, 3, 4, 7, 11}, {1, 2, 3, 9, 13}, {1, 2, 4, 8, 12}, {2, 3, 7, 9, 10}, {1, 4, 8, 9, 13}, {1, 5, 7, 8, 10}, {3, 7, 8, 12, 13}, {1, 2, 3, 4, 11}, {1, 2, 3, 7, 11}, {1, 2, 3, 7, 13}, {0, 1, 5, 8, 12}, {2, 6, 7, 9, 10}, {1, 4, 8, 9, 10}, {1, 5, 7, 8, 11}, {6, 7, 8, 11, 12}, {0, 2, 3, 4, 7}, {1, 2, 4, 7, 11}, {1, 2, 4, 7, 13}, {0, 1, 5, 8, 11}, {0, 6, 7, 9, 10}, {1, 5, 8, 9, 10}, {0, 6, 7, 8, 12}, {3, 7, 9, 12, 13}, {0, 1, 4, 5, 12}, {0, 2, 6, 8, 13}, {1, 2, 4, 8, 13}, {0, 1, 4, 8, 12}, {1, 4, 7, 9, 10}, {0, 5, 8, 9, 11}, {2, 3, 7, 9, 12}, {6, 7, 9, 11, 12}, {0, 2, 5, 8, 13}, {0, 1, 5, 9, 10}, {0, 1, 4, 8, 11}, {0, 5, 7, 9, 10}, {0, 6, 8, 9, 11}, {2, 6, 7, 9, 12}, {3, 8, 9, 12, 13}, {1, 2, 4, 5, 12}, {1, 2, 5, 8, 13}, {0, 1, 5, 7, 10}, {0, 5, 7, 9, 11}, {2, 4, 7, 8, 12}, {2, 3, 8, 9, 12}, {6, 8, 9, 11, 12}, {0, 1, 6, 9, 13}, {0, 1, 5, 7, 11}, {0, 6, 7, 9, 11}, {2, 3, 7, 8, 12}, {2, 6, 8, 9, 12}},
            5,
            {0, 1, 2, 3, 4, 5},
            QQ[x_0..x_841],
            matrix {{0, x_0, 1, 0, x_2, 0}, {0, 1, 0, 0, 0, x_53}, {0, 1, 0, 0, 0, 0}, {0, x_153, x_154, 1, 0, x_156}, {0, x_222, x_223, 1, x_225, 0}, {0, 1, x_289, 1, x_291, 0}, {0, x_354, 0, 0, 1, x_356}, {x_422, 0, x_423, 1, x_425, x_426}, {x_468, 0, x_469, 1, x_471, x_472}, {x_517, 0, 0, 1, 0, x_519}, {1, 0, 0, 0, 0, x_571}, {x_639, 0, x_640, 1, x_642, x_643}, {1, 0, 1, 1, 1, 0}, {x_776, 0, x_777, 1, x_779, 1}},
            graph(QQ[y_0..y_19], {{y_10, y_14}, {y_12, y_14}, {y_1, y_15}, {y_2, y_15}, {y_5, y_15}, {y_0, y_16}, {y_12, y_16}, {y_3, y_17}, {y_4, y_17}, {y_5, y_17}, {y_7, y_17}, {y_8, y_17}, {y_9, y_17}, {y_11, y_17}, {y_12, y_17}, {y_13, y_17}, {y_6, y_18}, {y_12, y_18}, {y_13, y_19}})
        );
    ) else if name == "Criado_Santos_P3513" then (
        return (
            {{0, 1, 2, 3, 4, 5, 6}, {7, 8, 9, 10, 11, 12, 13}, {0, 1, 5, 13, 7}, {0, 4, 7, 13, 8}, {5, 7, 8, 11, 10}, {0, 5, 7, 8, 13}, {2, 5, 8, 10, 9}, {4, 7, 8, 9, 10}, {2, 5, 7, 12, 8}, {5, 8, 9, 10, 11}, {2, 5, 7, 8, 11}, {5, 7, 9, 10, 11}, {0, 1, 5, 13, 6}, {0, 1, 4, 8, 13}, {0, 1, 4, 11, 9}, {1, 4, 8, 9, 13}, {0, 4, 8, 10, 9}, {0, 2, 5, 6, 13}, {0, 2, 5, 8, 13}, {0, 2, 4, 7, 12}, {0, 1, 3, 11, 9}, {1, 4, 7, 9, 13}, {1, 6, 8, 10, 9}, {3, 7, 8, 12, 13}, {1, 2, 5, 6, 13}, {1, 2, 5, 13, 8}, {0, 2, 4, 7, 11}, {0, 2, 3, 11, 9}, {1, 5, 7, 8, 13}, {1, 4, 7, 9, 11}, {2, 6, 8, 10, 9}, {6, 7, 8, 11, 12}, {0, 1, 3, 11, 4}, {0, 2, 6, 13, 8}, {1, 2, 4, 7, 12}, {0, 2, 5, 11, 9}, {0, 5, 7, 8, 10}, {0, 4, 7, 11, 9}, {4, 7, 8, 13, 9}, {0, 2, 3, 11, 4}, {1, 2, 6, 13, 8}, {1, 2, 4, 7, 11}, {0, 2, 5, 9, 10}, {0, 4, 7, 10, 8}, {0, 5, 7, 11, 9}, {2, 5, 8, 11, 9}, {1, 2, 3, 4, 11}, {0, 1, 6, 13, 8}, {1, 2, 3, 11, 7}, {0, 2, 3, 9, 10}, {1, 5, 7, 8, 12}, {1, 3, 7, 11, 9}, {2, 6, 8, 9, 11}, {0, 1, 2, 6, 10}, {0, 1, 6, 10, 8}, {0, 2, 5, 12, 7}, {0, 1, 3, 9, 10}, {1, 3, 7, 9, 13}, {2, 6, 7, 11, 9}, {0, 1, 2, 3, 10}, {0, 2, 6, 8, 10}, {0, 2, 5, 11, 7}, {1, 2, 3, 10, 9}, {1, 3, 8, 13, 9}, {2, 6, 7, 9, 12}, {3, 7, 9, 12, 13}, {0, 1, 4, 5, 12}, {0, 2, 5, 10, 8}, {1, 2, 3, 12, 7}, {1, 2, 6, 9, 10}, {2, 6, 7, 8, 12}, {2, 3, 7, 11, 9}, {2, 3, 7, 9, 12}, {3, 8, 9, 13, 12}, {0, 2, 4, 5, 12}, {0, 1, 5, 12, 7}, {1, 2, 5, 8, 12}, {1, 2, 6, 12, 9}, {2, 6, 7, 11, 8}, {0, 5, 7, 9, 10}, {1, 6, 8, 12, 9}, {6, 7, 9, 12, 11}, {1, 2, 4, 12, 5}, {0, 1, 4, 7, 12}, {1, 2, 6, 12, 8}, {1, 2, 3, 12, 9}, {1, 3, 7, 12, 8}, {0, 4, 7, 10, 9}, {1, 3, 8, 12, 9}, {6, 8, 9, 12, 11}, {0, 1, 4, 7, 13}, {0, 1, 4, 8, 9}, {1, 3, 7, 13, 8}, {0, 1, 8, 9, 10}},
            5,
            {0, 1, 2, 3, 4, 5},
            QQ[x_0..x_842],
            matrix {{0, x_0, 0, 0, 1, 0}, {0, x_52, 0, x_53, 1, x_55}, {0, 1, 1, 1, 1, 1}, {0, x_149, x_150, x_151, 1, x_153}, {0, x_219, x_220, 0, 1, x_222}, {0, 1, 0, x_288, 0, 0}, {0, x_351, x_352, x_353, 1, x_355}, {1, 0, 0, 0, 0, 0}, {x_470, 0, 1, 0, 0, 0}, {1, 0, x_521, x_522, 1, x_524}, {x_570, 0, x_571, x_572, 0, 1}, {x_640, 0, x_641, x_642, 0, 1}, {x_706, 0, x_707, x_708, 1, x_710}, {x_772, 0, 0, 0, 1, 0}},
            graph(QQ[y_0..y_19], {{y_7, y_14}, {y_9, y_14}, {y_2, y_15}, {y_5, y_15}, {y_2, y_16}, {y_8, y_16}, {y_2, y_17}, {y_0, y_18}, {y_1, y_18}, {y_2, y_18}, {y_3, y_18}, {y_4, y_18}, {y_6, y_18}, {y_9, y_18}, {y_12, y_18}, {y_13, y_18}, {y_2, y_19}, {y_10, y_19}, {y_11, y_19}})
        );
    ) else if name == "Criado_Santos_P2105" then (
        return (
            {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}, {14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27}, {0, 2, 3, 7, 14}, {0, 2, 3, 8, 14}, {0, 2, 7, 10, 14}, {1, 2, 6, 9, 15}, {0, 1, 6, 10, 15}, {0, 2, 6, 10, 15}, {0, 1, 3, 6, 17}, {1, 2, 3, 6, 17}, {0, 2, 6, 8, 17}, {0, 3, 6, 8, 17}, {2, 3, 6, 8, 17}, {0, 1, 3, 15, 17}, {0, 1, 6, 15, 17}, {0, 2, 6, 15, 17}, {1, 2, 6, 15, 17}, {0, 2, 8, 15, 17}, {0, 3, 8, 15, 17}, {2, 3, 8, 15, 17}, {2, 3, 5, 7, 18}, {2, 5, 7, 9, 18}, {3, 5, 7, 12, 18}, {0, 3, 7, 14, 19}, {2, 3, 7, 14, 19}, {0, 3, 8, 14, 19}, {0, 2, 10, 14, 19}, {0, 7, 10, 14, 19}, {2, 7, 10, 14, 19}, {0, 3, 8, 15, 19}, {1, 2, 5, 9, 20}, {0, 1, 4, 15, 20}, {2, 3, 5, 15, 20}, {1, 2, 9, 15, 20}, {1, 6, 9, 15, 20}, {2, 6, 9, 15, 20}, {1, 6, 10, 15, 20}, {2, 6, 10, 15, 20}, {3, 9, 13, 16, 20}, {1, 2, 3, 17, 20}, {1, 2, 15, 17, 20}, {2, 3, 15, 17, 20}, {1, 4, 15, 17, 20}, {3, 7, 15, 17, 20}, {2, 5, 9, 18, 20}, {0, 2, 16, 18, 20}, {2, 9, 16, 18, 20}, {9, 13, 16, 18, 20}, {0, 8, 15, 19, 20}, {0, 1, 3, 9, 21}, {0, 1, 4, 9, 21}, {0, 3, 7, 9, 21}, {0, 4, 9, 10, 21}, {1, 4, 9, 10, 21}, {2, 6, 9, 10, 21}, {0, 7, 9, 10, 21}, {2, 7, 9, 10, 21}, {3, 7, 9, 12, 21}, {0, 3, 7, 19, 21}, {2, 3, 7, 19, 21}, {0, 2, 10, 19, 21}, {0, 7, 10, 19, 21}, {2, 7, 10, 19, 21}, {1, 2, 3, 5, 22}, {1, 3, 5, 13, 22}, {1, 3, 9, 13, 22}, {1, 5, 9, 13, 22}, {3, 5, 11, 13, 22}, {5, 9, 11, 13, 22}, {1, 3, 15, 17, 22}, {1, 4, 15, 17, 22}, {3, 7, 15, 17, 22}, {1, 2, 3, 20, 22}, {1, 2, 5, 20, 22}, {2, 3, 5, 20, 22}, {1, 5, 9, 20, 22}, {3, 5, 11, 20, 22}, {5, 9, 11, 20, 22}, {3, 9, 13, 20, 22}, {3, 11, 13, 20, 22}, {9, 11, 13, 20, 22}, {1, 3, 17, 20, 22}, {1, 4, 17, 20, 22}, {3, 7, 17, 20, 22}, {4, 15, 17, 20, 22}, {2, 3, 14, 19, 23}, {3, 5, 14, 19, 23}, {2, 6, 14, 19, 23}, {2, 3, 14, 21, 23}, {3, 5, 14, 21, 23}, {2, 3, 19, 21, 23}, {3, 5, 19, 21, 23}, {5, 14, 19, 21, 23}, {0, 1, 4, 10, 24}, {3, 5, 9, 12, 24}, {5, 7, 9, 12, 24}, {0, 2, 8, 14, 24}, {2, 3, 8, 14, 24}, {2, 3, 5, 15, 24}, {0, 2, 8, 15, 24}, {2, 3, 8, 15, 24}, {0, 1, 10, 15, 24}, {0, 2, 10, 15, 24}, {2, 3, 5, 18, 24}, {2, 3, 7, 18, 24}, {2, 7, 9, 18, 24}, {5, 7, 9, 18, 24}, {3, 5, 12, 18, 24}, {3, 7, 12, 18, 24}, {5, 7, 12, 18, 24}, {0, 2, 16, 18, 24}, {2, 9, 16, 18, 24}, {9, 13, 16, 18, 24}, {0, 2, 14, 19, 24}, {3, 5, 14, 19, 24}, {2, 6, 14, 19, 24}, {0, 8, 14, 19, 24}, {3, 8, 14, 19, 24}, {3, 5, 15, 19, 24}, {3, 8, 15, 19, 24}, {0, 1, 4, 20, 24}, {0, 2, 10, 20, 24}, {0, 4, 10, 20, 24}, {1, 4, 10, 20, 24}, {3, 5, 11, 20, 24}, {5, 9, 11, 20, 24}, {3, 11, 13, 20, 24}, {9, 11, 13, 20, 24}, {0, 1, 15, 20, 24}, {2, 5, 15, 20, 24}, {0, 8, 15, 20, 24}, {1, 10, 15, 20, 24}, {2, 10, 15, 20, 24}, {3, 7, 16, 20, 24}, {3, 13, 16, 20, 24}, {0, 2, 18, 20, 24}, {2, 5, 18, 20, 24}, {5, 9, 18, 20, 24}, {9, 13, 18, 20, 24}, {13, 16, 18, 20, 24}, {0, 8, 19, 20, 24}, {8, 15, 19, 20, 24}, {5, 14, 19, 21, 24}, {5, 15, 19, 21, 24}, {3, 5, 9, 11, 25}, {3, 9, 11, 13, 25}, {3, 9, 13, 16, 25}, {0, 2, 10, 20, 25}, {2, 6, 10, 20, 25}, {0, 2, 16, 20, 25}, {0, 16, 18, 20, 25}, {3, 5, 9, 24, 25}, {3, 5, 11, 24, 25}, {5, 9, 11, 24, 25}, {3, 11, 13, 24, 25}, {9, 11, 13, 24, 25}, {0, 2, 16, 24, 25}, {3, 7, 16, 24, 25}, {2, 9, 16, 24, 25}, {3, 13, 16, 24, 25}, {9, 13, 16, 24, 25}, {0, 16, 18, 24, 25}, {0, 2, 19, 24, 25}, {2, 6, 19, 24, 25}, {0, 18, 20, 24, 25}, {0, 19, 20, 24, 25}, {1, 6, 9, 10, 26}, {0, 1, 3, 15, 26}, {0, 1, 4, 15, 26}, {0, 3, 15, 19, 26}, {3, 5, 15, 19, 26}, {1, 6, 9, 20, 26}, {2, 6, 9, 20, 26}, {0, 4, 10, 20, 26}, {1, 4, 10, 20, 26}, {1, 6, 10, 20, 26}, {0, 4, 15, 20, 26}, {3, 7, 16, 20, 26}, {2, 9, 16, 20, 26}, {3, 9, 16, 20, 26}, {0, 15, 19, 20, 26}, {0, 1, 3, 21, 26}, {0, 1, 4, 21, 26}, {2, 3, 7, 21, 26}, {1, 3, 9, 21, 26}, {2, 6, 9, 21, 26}, {2, 7, 9, 21, 26}, {0, 2, 10, 21, 26}, {0, 4, 10, 21, 26}, {1, 4, 10, 21, 26}, {2, 6, 10, 21, 26}, {1, 9, 10, 21, 26}, {6, 9, 10, 21, 26}, {3, 7, 12, 21, 26}, {3, 9, 12, 21, 26}, {7, 9, 12, 21, 26}, {3, 5, 15, 21, 26}, {0, 2, 19, 21, 26}, {0, 3, 19, 21, 26}, {3, 5, 19, 21, 26}, {5, 15, 19, 21, 26}, {1, 3, 9, 22, 26}, {1, 3, 15, 22, 26}, {1, 4, 15, 22, 26}, {3, 7, 15, 22, 26}, {1, 4, 20, 22, 26}, {3, 7, 20, 22, 26}, {1, 9, 20, 22, 26}, {3, 9, 20, 22, 26}, {4, 15, 20, 22, 26}, {2, 7, 9, 24, 26}, {3, 7, 12, 24, 26}, {3, 9, 12, 24, 26}, {7, 9, 12, 24, 26}, {7, 16, 20, 24, 26}, {3, 7, 16, 25, 26}, {2, 9, 16, 25, 26}, {3, 9, 16, 25, 26}, {2, 6, 20, 25, 26}, {0, 10, 20, 25, 26}, {6, 10, 20, 25, 26}, {2, 16, 20, 25, 26}, {0, 19, 20, 25, 26}, {2, 6, 24, 25, 26}, {3, 7, 24, 25, 26}, {2, 9, 24, 25, 26}, {3, 9, 24, 25, 26}, {7, 16, 24, 25, 26}, {3, 5, 15, 20, 27}, {3, 7, 15, 20, 27}, {7, 15, 17, 20, 27}, {2, 3, 14, 21, 27}, {3, 5, 14, 21, 27}, {3, 5, 15, 21, 27}, {7, 15, 17, 22, 27}, {7, 17, 20, 22, 27}, {2, 6, 14, 23, 27}, {2, 6, 19, 23, 27}, {6, 14, 19, 23, 27}, {2, 14, 21, 23, 27}, {2, 19, 21, 23, 27}, {2, 3, 7, 24, 27}, {2, 3, 14, 24, 27}, {3, 5, 14, 24, 27}, {2, 6, 14, 24, 27}, {6, 14, 19, 24, 27}, {3, 5, 20, 24, 27}, {3, 7, 20, 24, 27}, {5, 15, 20, 24, 27}, {5, 14, 21, 24, 27}, {5, 15, 21, 24, 27}, {0, 2, 10, 25, 27}, {2, 6, 10, 25, 27}, {0, 2, 19, 25, 27}, {2, 6, 19, 25, 27}, {6, 19, 24, 25, 27}, {2, 3, 7, 26, 27}, {0, 2, 10, 26, 27}, {2, 6, 10, 26, 27}, {3, 7, 15, 26, 27}, {0, 2, 19, 26, 27}, {2, 3, 21, 26, 27}, {3, 15, 21, 26, 27}, {2, 19, 21, 26, 27}, {7, 15, 22, 26, 27}, {7, 20, 22, 26, 27}, {2, 6, 24, 26, 27}, {2, 7, 24, 26, 27}, {7, 20, 24, 26, 27}, {0, 10, 25, 26, 27}, {6, 10, 25, 26, 27}, {0, 19, 25, 26, 27}, {6, 24, 25, 26, 27}},
            5,
            {0, 1, 157, 210, 224, 226},
            QQ[x_0..x_6261],
            matrix {{0, 1, x_108, x_145, x_157, x_159}, {0, 1, x_321, x_358, x_372, x_374}, {0, 1, x_514, 0, x_565, x_566}, {0, 1, 0, x_716, 0, 0}, {0, 1, x_903, x_946, x_960, x_962}, {0, 1, x_1125, x_1174, x_1188, x_1190}, {0, 1, 1, 1, 1, x_1424}, {0, 1, 0, 0, 0, 1}, {0, 1, x_1813, x_1866, x_1880, x_1882}, {0, 1, x_2043, 0, x_2088, 0}, {0, 1, x_2265, x_2308, x_2320, x_2322}, {0, 1, x_2503, x_2556, x_2570, x_2572}, {0, 1, x_2768, x_2818, x_2829, x_2831}, {0, 1, x_3014, x_3065, x_3079, x_3081}, {x_3128, 0, x_3261, x_3314, x_3328, 1}, {1, 0, x_3482, x_3523, x_3537, 1}, {x_3576, 0, 0, x_3763, x_3772, 1}, {x_3820, 0, x_3952, x_4005, x_4019, 1}, {x_4065, 0, x_4198, x_4249, x_4263, 1}, {x_4312, 0, x_4438, x_4481, x_4494, 1}, {x_4533, 0, x_4631, x_4667, x_4675, 1}, {x_4715, 0, x_4850, x_4883, x_4897, 1}, {x_4936, 0, x_5070, x_5114, x_5128, 1}, {x_5173, 0, x_5321, x_5374, x_5388, 1}, {1, 0, 0, 0, 0, 0}, {x_5613, 0, 0, 1, 0, 0}, {x_5841, 0, 1, 0, 0, 0}, {x_6034, 0, x_6190, x_6243, x_6257, 1}},
            graph(QQ[y_0..y_33], {{y_15, y_28}, {y_24, y_28}, {y_0, y_29}, {y_1, y_29}, {y_2, y_29}, {y_3, y_29}, {y_4, y_29}, {y_5, y_29}, {y_6, y_29}, {y_7, y_29}, {y_8, y_29}, {y_9, y_29}, {y_10, y_29}, {y_11, y_29}, {y_12, y_29}, {y_13, y_29}, {y_6, y_30}, {y_26, y_30}, {y_6, y_31}, {y_25, y_31}, {y_6, y_32}, {y_7, y_33}, {y_14, y_33}, {y_15, y_33}, {y_16, y_33}, {y_17, y_33}, {y_18, y_33}, {y_19, y_33}, {y_20, y_33}, {y_21, y_33}, {y_22, y_33}, {y_23, y_33}, {y_27, y_33}})
        );
    ) else error("Sphere not found in database");
);


---------------------------------------------------------------------------------------------------------
-- CHECK CERTIFICATE


checkCertificate = method(Options => {Certificate => {}, Prismatoid => false})
checkCertificate (String, ZZ, ZZ) := ZZ => opts -> (fileName, nIter, maxDeg) -> (
    (F, d, fl, ringS, S1, T) := (0, 0, 0, 0, 0, 0);
    if opts.Prismatoid then (
        (F, d, fl, ringS, S1, T) = value get(fileName | "_data.txt");
    ) else (F, d, fl, ringS, S1, T) = sphere(fileName);
    numVerts := #unique flatten F;
    Fbases := hashTable value get(fileName | "_Fbases.txt");
    orientationFileName := fileName | "_orientation.txt";
    orientation := hashTable value get orientationFileName;

    certificate := opts.Certificate;
    if certificate == {} then (
        certificateFileName := fileName | "_iter" | toString(nIter) | "_deg" | toString(maxDeg) | "_finalCertificate_sage.txt";
        certificate = value get certificateFileName;
    );

    slackCertificate := sum apply(
        certificate, 
        c -> product apply(
            c, e -> orientation#(e_1 - 1) * det(S1^(join(Fbases#(e_1-1), {e_0-1})))
        )
    );
    if slackCertificate == 0 then (
        print("Certificate sums to 0!");
    ) else (
        print("Certificate does NOT sum to 0!");
    );
);


reorderSphere = (fileName, otherFacetsInCertificate) -> (
    (F, d, fl, ringS, S1, T) := sphere2(fileName);
    print("Old data:");
    print(toString F);
    print("Flag : " | toString(F_fl));
    print("Other facets in certificate : " | toString otherFacetsInCertificate);
    print(toString ringS);
    print(toString T);
    facetsInFlag := F_fl;

    newF := facetsInFlag | otherFacetsInCertificate | (F - set(facetsInFlag | otherFacetsInCertificate));
    S := symbolicSlackMatrix(newF, Object => "abstractPolytope");
    newRingS := ring S;
    print("\nNew data");
    print(toString newF);
    print("Is #F == #newF ? " | toString(#F == #newF));
    print("Is F == newF ? " | toString(set(F) === set(newF)));
    print(toString(F_fl));
    print(toString newRingS | "\n");

    reducedSlackMatrix := S_{0..d};
    (newS1, newT) := (matrix {{0}}, {});
    count := 0;
    positionsOnesS1 := apply(entries S1, e -> positions(e, f -> f == 1));
    positionsOnesNewS1 := apply(entries newS1, e -> positions(e, f -> f == 1));
    while (positionsOnesS1 != positionsOnesNewS1) and count < 50 do (
        setRandomSeed count;
        (newS1, newT) = setOnesForest(reducedSlackMatrix);
        count = count + 1;
    );
    if count == 50 then (
        print("Could not find same scaling!");
        newS1 = new MutableMatrix from reducedSlackMatrix;
        for i to numgens target S1 - 1 do (
            for j to numgens source S1 - 1 do (
                if S1_(i, j) == 1 then newS1_(i, j) = 1;
            );
        );
        newS1 = matrix(newS1);
        newT = T;
    );
    print(S1, newS1);
    print("\n" | toString newT);    

    return(newF, d, toList(0..d), newRingS, newS1, newT);
);


-----------------------------------------------------------------------------------------------
-- RECONSTRUCT INFEASIBILITY CERTIFICATE PRODUCED BY MATLAB

pairSort = p -> (
  -- INPUT: sequence p of two lists
  -- OUTPUT: two lists of p sorted according to the order of the first list
  indexFirst := apply(toList(0..#p_0-1), i->{(p_0)_i,i});
  indexFirst = sort indexFirst;
  newFirst := apply(indexFirst, e->e_0);
  perm := apply(indexFirst, e->e_1);
  newSecond := (p_1)_perm;
  return (newFirst,newSecond);
);


positionInMatrix = method()
positionInMatrix (Matrix, RingElement) := (ZZ, ZZ) => (S, m) -> (
    -- INPUT: S = matrix
    --        m = element to test
    -- OUTPUT: pair (i,j) such that S_(i,j) == m
    r := numgens target S;
    c := numgens source S;
    entriesS := flatten entries S;
    newM := substitute(m, ring S);
    posNewM := position(entriesS, e -> e == newM);
    try (
        row := posNewM // c;
        col := posNewM % c;
        return (row, col);
    ) else error("Element not found in matrix.");
);
positionInMatrix (Matrix, RingElement, List) := (ZZ, ZZ) => (S, m, flag) -> (
    -- INPUT: S = matrix
    --        m = element to test
    -- OUTPUT: pair (i,j) such that S_(i,j) == m
    r := numgens target S;
    c := numgens source S;
    entriesS := flatten entries S;
    newM := substitute(m, ring S);
    posNewM := position(entriesS, e -> e == newM);
    try (
        row := posNewM // c;
        col := flag_(posNewM % c);
        return (row, col);
    ) else error("Element not found in matrix.");
);


getHomogeneousCertificate = (data, slackCertificate) -> (
    -- INPUT: data = (F, fl, S), where F is the list of facets as vertex labels, fl is a flag, S is the symbolic slack matrix
    --        slackCertificate =
    -- OUTPUT: homogeneousCertificate = list of homogeneous polynomials in the infeasibility certificate,
    --                                  computed as certain minors of S_fl
    (F, fl, S) := data;
    homogeneousCertificate := {};
    for p in slackCertificate do (
        homConstraint := {};
        for q in p_1 do (
            homConstraint = homConstraint | {S_(q_0, q_1)};
        );
        homogeneousCertificate = homogeneousCertificate | {product(homConstraint)};
    );
    return homogeneousCertificate;
);


unpackHomogeneousCertificate = (polysCertificate, homogeneousCertificate, numRowsColsCertificate, varsS1) -> (
    -- INPUT: polysCertificate = list of lists, each containing the terms of a dehomogenized polynomial
    --                           in the infeasibility certificate
    --        homogeneousCertificate = list of homogeneous polynomials in the infeasibility certificate,
    --                                 computed as certain minors of S_fl
    --        numRowsColsCertificate = pair of integers, number of rows and columns in certificate
    --        varsS1 = list, variables in the dehomogenized matrix S1
    -- OUTPUT: finalCertificateList = list of lists, each containing the terms of a rehomogenized polynomial
    --                                in the infeasibility certificate
    --         listMonsFinalCertificate = list of homogeneous monomials in rehomogenized certificate
    varsS1 = apply(varsS1, v -> substitute(v, ring(homogeneousCertificate_0)));
    (numRowsCertificate, numColsCertificate) := numRowsColsCertificate;
    finalCertificateList := {};
    termsHomogCertificate := apply(homogeneousCertificate, p -> terms p);
    listMonsFinalCertificate := new MutableHashTable from {};
    for i to numRowsCertificate-1 do (
        finalCertificateP := {};
        termsHomogCertI := termsHomogCertificate_i;
        for j to numColsCertificate-1 do (
            m := (polysCertificate_i)_j;
            if m == 0 then (
                finalCertificateP = finalCertificateP | {0};
            ) else (
                newM := substitute(m, ring homogeneousCertificate_0);
                (signlessM, signM) := satPolyPair(newM);
                reorderedMons := {};
                if (degree(newM))_0 != 0 then (
                    nonConstantMon := select(termsHomogCertI, e -> (e//newM != 0 and set(support(e//newM)) * set(varsS1) === set{}) and signM == (satPolyPair(e))_1);
                    termsHomogCertI = termsHomogCertI - set(nonConstantMon);
                    if not listMonsFinalCertificate#?j then (
                        listMonsFinalCertificate#j = (satPolyPair(nonConstantMon_0))_0;
                    );
                    reorderedMons = reorderedMons | nonConstantMon;
                ) else (
                    -- here termsHomogCertI only contains one monomial, corresponding to the rehomogenization of the constant term
                    if not listMonsFinalCertificate#?j then listMonsFinalCertificate#j = (satPolyPair(termsHomogCertI_0))_0;
                    reorderedMons = reorderedMons | termsHomogCertI;
                );
                finalCertificateP = finalCertificateP | reorderedMons;
            );
        );
        if #finalCertificateP != numColsCertificate then error("Number of monomials in constraint " | toString(i) | " does not match number of monomials in dehomogenized certificate");
        finalCertificateList = finalCertificateList | {finalCertificateP};
    );
    print("\nList of monomials in rehomogenized certificate");
    listMonsFinalCertificate = for i to #keys(listMonsFinalCertificate)-1 list listMonsFinalCertificate#i;
    print(toString listMonsFinalCertificate);
    return(finalCertificateList, listMonsFinalCertificate);
);


getMultiplyingMonomials = (finalCertificateList, numRowsCertificate, numColsCertificate) -> (
    -- INPUT: finalCertificateList = list of lists, each containing the terms of a rehomogenized polynomial
    --                               in the infeasibility certificate
    --        numRowsCertificate = integer, number of rows in certificate
    --        numColsCertificate = integer, number of columns in certificate
    -- OUTPUT: list of monomials to multiply to rehomogenized constraints so that they sum to zero
    multiplyingMonomials := new MutableList from for i to numRowsCertificate-1 list 1;
    finalCertificate := apply(toList(0..numRowsCertificate-1), i -> (multiplyingMonomials#i, finalCertificateList_i));
    sumFinalCertificate := sum(apply(finalCertificate, f -> f_0*sum(f_1)));
    counter := 0;
    newFinalCertificateList := new MutableList from finalCertificateList;
    while sumFinalCertificate != 0 and counter < 20 do (
        for j to numColsCertificate-1 do (
            -- lcm of nonzero elements in column j
            lcmCol := lcm(apply(toList(newFinalCertificateList), p -> (if p_j != 0 then p_j else 1)));
            for i to numRowsCertificate-1 do (
                mon := (newFinalCertificateList#i)_j;
                if mon != 0 then (
                    multiplyingMon := (satPolyPair(multiplyingMonomials#i * lcmCol//mon))_0;
                    if multiplyingMon != multiplyingMonomials#i then (
                        newMultiplyingMon = multiplyingMon // multiplyingMonomials#i;
                        newFinalCertificateList#i = apply(newFinalCertificateList#i, m -> newMultiplyingMon * m);
                        multiplyingMonomials#i = multiplyingMon;
                    );
                );
            );
        );
        sumFinalCertificate = sum(apply(toList(newFinalCertificateList), f -> sum(f)));
        counter = counter + 1;
    );
    if counter == 20 then error("\nRehomogenized certificate does NOT sum to zero (after 20 iterations)!");
    return toList(multiplyingMonomials);
);


getReconstructedSlackMatrix = method(Options => {RedundantNonsimplicialFacets => false})
getReconstructedSlackMatrix (Sequence, List) := Matrix => opts -> (data, orientation) -> (
    -- INPUT: data = (F, d, fl, oldS), where
    --        F = list of facets
    --        d = dimension
    --        fl = a flag of facets
    --        oldS = original slack matrix
    --        orientation = list containing facets orientation (as pairs (col index, sign +1/-1))
    -- OUTPUT: reconstructed slack matrix with correct signs
    (F, d, fl, oldS) := data;
    ringS := ring oldS;
    numVerts := numgens target oldS;
    restF := F - set(F_fl);
    SF := oldS_fl;

    print("Reconstructing slack matrix");
    -- Find out which sets of vertices span the nonsimplicial facets in our choice of flag
    nonsimplicialFacets := select(F, f -> #f != d);
    numNonsimplFacets := #nonsimplicialFacets;
    indNonsimplFacets := apply(nonsimplicialFacets, f -> position(F, e -> e==f));

    M := {};
    if numNonsimplFacets != 0 then (
        -- Form slack and reduced slack matrix of sphere
        transposeOldS := transpose oldS;

        basesNonsimplFacets := {};
        Fbases := new MutableList from F;

        if not opts.RedundantNonsimplicialFacets then (
            for i to numNonsimplFacets-1 do (
                basesFi := {};
                subsetsFi := subsets(F_(position(F, j -> j == nonsimplicialFacets_i)), d);
                for s in subsetsFi do (
                    try rfl = findFlag(transposeOldS_s) else continue;
                    -- collect bases of F_i
                    if #rfl == d then (
                        basesFi = s;
                        break;
                    );
                );
                if basesFi === {} then (
                    error("No basis found for nonsimplicial facet " | toString(nonsimplicialFacets_i));
                );
                basesNonsimplFacets = basesNonsimplFacets | {basesFi};
            );
            print("Bases of nonsimplicial facets : " | toString(basesNonsimplFacets));

            for j to numNonsimplFacets-1 do (
                Fbases#(indNonsimplFacets_j) = basesNonsimplFacets_j;
            );

        ) else ( -- adding redundant columns, one for each facet basis of a nonsimplicial facet
            for i to numNonsimplFacets-1 do (
                basesFi := {};
                subsetsFi := subsets(F_(position(F, j -> j == nonsimplicialFacets_i)), d);
                for s in subsetsFi do (
                    try rfl = findFlag(transposeOldS_s) else continue;
                    -- collect bases of F_i
                    if #rfl == d then (
                        basesFi = basesFi | {s};
                    );
                );
                if basesFi === {} then (
                    error("No basis found for nonsimplicial facet " | toString(nonsimplicialFacets_i));
                );
                basesNonsimplFacets = basesNonsimplFacets | {basesFi};
            );
            print("Bases of nonsimplicial facets : " | toString(basesNonsimplFacets));

            for j to numNonsimplFacets-1 do (
                Fbases#(indNonsimplFacets_j) = (basesNonsimplFacets_j)_0;
                Fbases = join(Fbases, (basesNonsimplFacets_j)_{1..#basesNonsimplFacets_j-1});
            );
            print("Total number of columns including redundant ones: " | toString(#Fbases));
        );

    ) else (
        print("\nSimplicial sphere");
    );

    print("\nReconstructing slack matrix.");
    Fbases = toList(Fbases);
    -- Reconstruct matrix from facet bases
    elapsedTime M = matrix for i to numVerts-1 list for j to #Fbases-1 list (orientation_j)_1 * det(SF^(join(Fbases_j, {i})));
    M = substitute(M, ringS);

    return (Fbases, M);
);


reconstructCertificate = method(Options => {Flag => {}, NewSphere => false, Prismatoid => false, OldPrismatoid => false, Suffix => "", ExternalListMons => {}, RedundantNonsimplicialFacets => false})
reconstructCertificate (String, ZZ, ZZ) := String => opts -> (fileName, nIter, maxDeg) -> (
    -- INPUT: fileName = string containing name of sphere, e.g., "Altshuler_M963"
    --        nIter = integer, number of iterations in products
    --        maxDeg = integer, maximum degree of original constraints
    -- OUTPUT: list certificatePolys of polynomials in infeasibility certificate

    fullFileName := fileName | "_iter" | toString nIter | "_deg" | toString maxDeg;
    if opts.Suffix != "" then fullFileName = fullFileName | "_" | opts.Suffix;

    orientationFile := fileName | "_orientation";
    if opts.RedundantNonsimplicialFacets then (
        orientationFile = orientationFile | "_redundant_facets.txt";
    ) else orientationFile = orientationFile | ".txt";

    certificateFile := fullFileName | "_certificate.txt";
    listMonsFile := fullFileName | "_listMons.txt";
    slackEntriesFile := fullFileName | "_constraints.txt";

    -- load data
    (F, d, fl, ringS, S1, T) := (0, 0, 0, 0, 0, 0);
    if opts.NewSphere then (
        (F, d, fl, ringS, S1, T) = value get(fullFileName | "_data.txt");
    ) else (F, d, fl, ringS, S1, T) = sphere(fileName);
    if opts.Prismatoid then (
        if opts.OldPrismatoid then (
            (F, d, fl, ringS, S1) = value get(fileName | "_data.txt");
        ) else  (F, d, fl, ringS, S1, T) = value get(fileName | "_data.txt");
    );

    varsS1 := (unique flatten entries S1 - set{0_(ring S1), 1_(ring S1)});
    print("\nVars in S1 = " | toString varsS1);
    print("\nFlag = " | toString(fl));
    oldS := symbolicSlackMatrix(F, Object => "abstractPolytope");
    orientation := value get orientationFile;

    -- reconstruct slack matrix by Pluecker coordinates
    -- notice that the reconstructed entries already have the right sign using orientation
    elapsedTime (Fbases, S) := getReconstructedSlackMatrix((F, d, fl, oldS), orientation, RedundantNonsimplicialFacets => opts.RedundantNonsimplicialFacets);

    -- read Matlab matrix certificate and transform matrix into list of lists of numbers
    listMatrix := apply(lines(get certificateFile), s -> apply(separate(" ", s), z -> value z));

    -- first list contains indices of monomials in certificate
    idxMonsCertificate := listMatrix_0;
    numMons := #idxMonsCertificate;

    -- other lists contain coefficients of polynomials in certificate
    -- (the last coefficient in each list is the constant term)
    coeffsPolysCertificate :=  drop(listMatrix, 1);

    -- get list of monomials occurring in each certificate to do key search in constraint hash table
    -- include last coefficient for constant even if zero
    nonzeroCoeffsPositions := apply(coeffsPolysCertificate, c -> unique join(positions(c, e -> e!=0), {numMons-1}));
    slackEntryKeys := apply(toList(0..#nonzeroCoeffsPositions-1),
        i -> (idxMonsCertificate_(nonzeroCoeffsPositions_i), (coeffsPolysCertificate_i)_(nonzeroCoeffsPositions_i)));

    -- read hashTable of constraints as slack entry indices
    constraintTable := value get slackEntriesFile;
    -- rekey with monomial indices in sorted order
    constraintTable = applyKeys(constraintTable, pairSort);
    slackCertificate := apply(slackEntryKeys, k -> constraintTable#k);

    for p in slackCertificate do (
        print(toString(p, product(apply(p_1, z->S_z))));
    );

    -- read list of monomials from file
    listMons := {};
    print("\nReading list of monomials");
    if opts.ExternalListMons == {} then (
        listMons = value get listMonsFile;
    ) else listMons = opts.ExternalListMons;

    -- indices of variables in certificate
    indicesVars := flatten apply(idxMonsCertificate_{0..numMons-2}, i -> keys(listMons_(i-1)));

    -- create list of monomials in certificate
    monsCertificate := for i in idxMonsCertificate_{0..numMons-2} list product(apply(keys(listMons_(i-1)), m -> x_m^((listMons_(i-1))#m)));
    -- adding 1 as "monomial" of the constant term
    monsCertificate = monsCertificate | {1_ringS};

    print("\nDehomogenized infeasibility certificate: " | toString(#slackCertificate) | " constraints with " | toString(#monsCertificate) | " terms");

    -- create list of polynomials in certificate
    polysCertificate := for j to #coeffsPolysCertificate-1 list (for i to numMons-1 list (coeffsPolysCertificate_j)_i * (slackCertificate_j)_0 * monsCertificate_i);
    for p in polysCertificate do print(toString p);

    print("\nSlack entries in certificate:");
    print(toString slackCertificate);

    originalCertificate := for i to #polysCertificate-1 list substitute(sum(polysCertificate_i), ringS) * (slackCertificate_i)_0;

    print("\nRehomogenizing certificate");
    restCols := toList(#fl..#F-1);
    restColsF := toList(0..#F-1) - set(fl);

    homogeneousCertificate := getHomogeneousCertificate((F, fl, S), slackCertificate);

    numRowsCertificate := #polysCertificate;
    numColsCertificate := #polysCertificate_0;

    print("Unpacking rehomogenized certificate");
    (finalCertificateList, listMonsFinalCertificate) := unpackHomogeneousCertificate(polysCertificate, homogeneousCertificate, (numRowsCertificate, numColsCertificate), varsS1);

    print("\nComputing multiplying factors");
    multiplyingMonomials := getMultiplyingMonomials(finalCertificateList, numRowsCertificate, numColsCertificate);

    finalCertificate := apply(toList(0..numRowsCertificate-1), i -> (multiplyingMonomials#i, finalCertificateList_i));

    print("\nFinal certificate");
    for p in finalCertificate do print(toString p);

    positionCertificate := {};
    for i to numRowsCertificate-1 do (
        if multiplyingMonomials_i != 1 then (
            varsInMon := flatten apply(pairs((keys(standardForm multiplyingMonomials_i))_0), p -> toList(p_1 : x_(p_0)));
            posMultiplyingMons := apply(varsInMon, e -> positionInMatrix(oldS_fl, e, fl));
            positionCertificate = positionCertificate | {{(slackCertificate_i)_1, posMultiplyingMons}};
        ) else (
            positionCertificate = positionCertificate | {{(slackCertificate_i)_1, {}}};
        );
    );
    print("\nFlag = " | toString(fl));
    print("Position of elements in certificate");
    for p in positionCertificate do print("constraint = " | toString(p_0) | ", multiplying factors = " | toString(sort p_1));

    cardFinalCertificate := #(unique apply(positionCertificate, p -> #p_0+#p_1));
    if cardFinalCertificate > 1 then print("\nWarning: final certificate is NOT homogeneous!");

    colIdxCertificate := unique flatten apply(positionCertificate, c -> apply(c_0, e -> e_1));
    facetsCertificate := new HashTable from apply(colIdxCertificate, i -> (i => Fbases_i));
    print("\nFacets involved in certificate");
    print(toString(sort for k in keys(facetsCertificate) list (k => facetsCertificate#k)));

    finalCertificateFile := fullFileName | "_finalCertificate.txt";
    print("\nDumping sparse hash table of constraints to file " | finalCertificateFile);
    f := finalCertificateFile << "";
    f << "(" << endl;
    f << toString polysCertificate | "," << endl;
    f << toString slackCertificate | "," << endl;
    f << toString listMonsFinalCertificate | "," << endl;
    f << toString positionCertificate | "," << endl;
    f << toString finalCertificate | "," << endl;
    f << toString facetsCertificate << endl;
    f << ");" << endl;
    f << close;

    return (toString finalCertificate);
);


findSlackMatrixEntry = method(Options => {Prismatoid => false})
findSlackMatrixEntry (String, List) := Sequence => opts -> (fileName, entriesPosition) -> (
    -- INPUT: fileName = string containing name of sphere, e.g., "Altshuler_M963"
    --        entriesPosition = list of pairs representing the position of variables in the reconstructed slack matrix
    -- OUTPUT: position of element in the reconstructed slack matrix
    (F, d, fl, ringS, S1, T) := (0, 0, 0, 0, 0, 0);
    if opts.Prismatoid then (
        -- recall prismatoid data
        (F, d, fl, ringS, S1, T) = value get(fileName | "_data.txt");
    ) else if not opts.Prismatoid then (
        -- recall sphere data
        (F, d, fl, ringS, S1, T) = sphere(fileName);
    );
    S := symbolicSlackMatrix(F, Object => "abstractPolytope");
    R := ring S1;
    restF := F - set(F_fl);
    elapsedTime M := reconstructSlackMatrix(S_fl, restF);
    restCols := toList(#fl..#F-1);
    restColsF := toList(0..#F-1) - set(fl);
    reverseMappingHash := hashTable join(
        for i to #fl-1 list i => fl_i,
        for j to #restCols-1 list (j + d + 1) => restColsF_j
    );
    return positionInMatrix(M, product(apply(entriesPosition, a -> M_a)));
);


-- Relabel certificate for the paper
-- Relabel facets and vertices starting from 1 and slack entries as x_(i,j)
relabelCertificate = method(Options => {Prismatoid => false, OldPrismatoid => false, NewSphere => false, Suffix => "", RedundantNonsimplicialFacets => false})
relabelCertificate (String, ZZ, ZZ) := Sequence => opts -> (fileName, nIter, maxDeg) -> (
  -- INPUT: sequence p of two lists
  -- OUTPUT: two lists of p sorted according to the order of the first list
    fullFileName := fileName | "_iter" | toString(nIter) | "_deg" | toString(maxDeg);
    if opts.Suffix != "" then fullFileName = fullFileName | "_" | opts.Suffix;

    (F, d, fl, ringS, S1, T) := (0, 0, 0, 0, 0, 0);
    if opts.NewSphere then (
        (F, d, fl, ringS, S1, T) = value get(fullFileName | "_data.txt");
    ) else (F, d, fl, ringS, S1, T) = sphere(fileName);
    if opts.Prismatoid then (
        if opts.OldPrismatoid then (
            (F, d, fl, ringS, S1) = value get(fileName | "_data.txt");
        ) else  (F, d, fl, ringS, S1, T) = value get(fileName | "_data.txt");
    );

    S := substitute(symbolicSlackMatrix(F, Object => "abstractPolytope"), ringS);
    Sfl := S_fl;


    orientationFile := fileName | "_orientation";
        if opts.RedundantNonsimplicialFacets then (
            orientationFile = orientationFile | "_redundant_facets.txt";
        ) else orientationFile = orientationFile | ".txt";
    orientation := value get(orientationFile);
    certificate := value get(fullFileName |"_finalCertificate.txt");

    facetsInFlag := apply(fl, e -> (e, F_e));
    orientationFacetsInFlag := new HashTable from select(orientation, x -> member(x_0, fl));
    orientedFacetsInFlag := sort apply(facetsInFlag, f -> if orientationFacetsInFlag#(f_0) == -1 then (f_0, switch(#(f_1)-2, #(f_1)-1, f_1)) else f);

    facetsInCert := pairs certificate_5;
    orientationFacetsInCert := new HashTable from select(orientation, x -> member(x_0, keys certificate_5));
    orientedFacetsInCert := sort apply(facetsInCert, f -> if orientationFacetsInCert#(f_0) == -1 then (f_0, switch(#(f_1)-2, #(f_1)-1, f_1)) else f);

    neededFacets := orientedFacetsInFlag | (orientedFacetsInCert - set(orientedFacetsInFlag));
    print("neededFacets : " | toString neededFacets);
    relabeledNeededFacets := apply(neededFacets, f -> (position(neededFacets, g -> g==f) + 1, apply(f_1, e -> e+1)));
    print("relabeledNeededFacets : " | toString relabeledNeededFacets);
    hashNeededFacets := new HashTable from apply(neededFacets, f -> (f_0, position(neededFacets, g -> g==f) + 1));
    print("hashNeededFacets : " | toString hashNeededFacets);

    (numRowsS1, numColsS1) := (numgens target S1, numgens source S1);
    newVars := x_(1, 1)..x_(numRowsS1, numColsS1);
    allVarsRing := QQ[gens ringS | toList newVars];
    relabeledSfl := mutableMatrix(allVarsRing, numRowsS1, numColsS1);
    relabeledS1 := mutableMatrix(allVarsRing, numRowsS1, numColsS1);
    relabeledVars := {};
    for h in ((0, 0)..(numRowsS1-1, numColsS1-1)) do (
        e := S1_h;
        if e != 0 then (
            relabeledSfl_h = x_(h_0+1, h_1+1);
            relabeledVars = relabeledVars | {(Sfl_h, x_(h_0+1, h_1+1))};
            if (degree(e))_0 == 1 then (
                relabeledS1_h = x_(h_0+1, h_1+1);
            ) else if e == 1 then relabeledS1_h = 1;
        );
    );
    relabeledS1 = matrix relabeledS1;

    relabeledVars = new HashTable from relabeledVars;
    print("\nrelabeledVars : " | toString relabeledVars);

    print("\nS_fl :");
    print(Sfl);
    print("\nrelabeled S_fl :");
    print(relabeledSfl);
    print("\nrelabeled S1 :");
    print(relabeledS1);

    dehomogenizedCertificate := certificate_1;
    print("\ndehomogenizedCertificate : " | toString(dehomogenizedCertificate));
    relabeledDehomogenizedCertificate := apply(dehomogenizedCertificate, p -> {p_0, apply(p_1, q -> (q_0+1, hashNeededFacets#(q_1))), p_2});
    print("relabeledDehomogenizedCertificate : " | toString(relabeledDehomogenizedCertificate));

    multiplyingMons := apply(certificate_4, p -> if class(p_0)===String then value(p_0) else p_0);
    print("\nmultiplyingMons : " | toString(multiplyingMons));
    relabeledMultiplyingMons := apply(multiplyingMons, f -> sub(sub(f, allVarsRing), apply(keys relabeledVars, h -> sub(h, allVarsRing) => sub(relabeledVars#h, allVarsRing)) ) );
    print("relabeledMultiplyingMons : " | toString relabeledMultiplyingMons);

    homogeneousCertificate := certificate_3;
    relabeledHomogeneousCertificate := apply(homogeneousCertificate, f -> apply(f_0, e -> (e_0+1, hashNeededFacets#(e_1))) | {relabeledMultiplyingMons_(position(homogeneousCertificate, g -> g==f))});
    print("\nhomogeneousCertificate : " | toString homogeneousCertificate);
    print("relabeledHomogeneousCertificate : " | toString relabeledHomogeneousCertificate);

    return (relabeledNeededFacets, toString relabeledVars, toString relabeledS1, toString relabeledHomogeneousCertificate);
);


relabelData = method(Options => {RedundantNonsimplicialFacets => false, Certificate => {}})
relabelData (String, ZZ, ZZ) := Sequence => opts -> (fileName, nIter, maxDeg) -> (
    -- INPUT: fileName = string containing name of sphere
    --        nIter = integer, number of factors in prodcuts of constraints
    --        maxDeg = integer, maximum degree in selected constraints
    -- OUTPUT: (orientedFacets, d, fl, relabeledS1, certificate) = sequence containing
    --          orientedFacets = list of positively oriented facets
    --          d = integer, dimension of sphere
    --          fl = list of facet indices forming a flag
    --          relabeledS1 = matrix, scaled reduced slack matrix whose non-constant entries are variables x_{(i,j)}
    --          certificate = list of lists, each representing the product of entries of reconstructed slack matrix
    (F, d, fl, ringS, S1, T) := value get(fileName | "_data.txt");

    -- Creating list of oriented facets
    Fbases := hashTable value get(fileName | "_Fbases.txt");

    orientationFile := fileName | "_orientation";
    if opts.RedundantNonsimplicialFacets then (
        orientationFile = orientationFile | "_redundant_facets.txt";
    ) else orientationFile = orientationFile | ".txt";
    orientation := hashTable value get orientationFile;

    newF := apply(F, f -> apply(f, e -> e+1));
    newFbases := applyPairs(Fbases, (k, v) -> (k, apply(v, e -> e+1)));
    orientedFacets := {};
    for i to #newF-1 do (
        f := newFbases#i;
        if orientation#i == -1 then f = switch(#(f)-2, #(f)-1, f);
        if #F_i != d then (
            orientedFacets = orientedFacets | {f | (apply(F_i, e -> e+1) - set(f))};
        ) else orientedFacets = orientedFacets | {f};
    );

    -- Relabeling flag
    newFl := apply(fl, i -> i + 1);

    -- Relabeling reduced slack matrix S1
    (numRowsS1, numColsS1) := (numgens target S1, numgens source S1);
    newVars := x_(1, 1)..x_(numRowsS1, numColsS1);
    allVarsRing := QQ[gens ringS | toList newVars];
    relabeledS1 := mutableMatrix(allVarsRing, numRowsS1, numColsS1);
    for h in ((0, 0)..(numRowsS1-1, numColsS1-1)) do (
        e := S1_h;
        if e != 0 then (
            if (degree(e))_0 == 1 then (
                relabeledS1_h = x_(h_0+1, h_1+1);
            ) else if e == 1 then relabeledS1_h = 1;
        );
    );
    relabeledS1 = matrix relabeledS1;

    -- Relabeling certificate
    certificate := opts.Certificate;
    if certificate == {} then (
        certificateFileName := fileName | "_iter" | toString(nIter) | "_deg" | toString(maxDeg) | "_finalCertificate_sage.txt";
        certificate = value get certificateFileName;
    );

    finalDataFile := fileName | "_finalData.txt";
    print("\nDumping final data to file " | finalDataFile);
    f := finalDataFile << "";
    f << "(" << endl;
    f << toString orientedFacets | "," << endl;
    f << toString d | "," << endl;
    f << toString newFl | "," << endl;
    f << toString relabeledS1 | "," << endl;
    f << toString certificate << endl;
    f << ");" << endl;
    f << close;

    return(orientedFacets, d, newFl, relabeledS1, certificate);    
);
