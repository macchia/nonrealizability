"""Sage interface to Macaulay2 and Gurobi for non-realizability certificates of combinatorial polytopes."""

import argparse
import copy as cp
import csv
from itertools import chain, product as iter_prod
import numpy as np
import os
import sys
import logging
import datetime as dt
import random
import re
import time
import gc

from sage.all import (
    macaulay2,
    det,
    product,
    matrix,
    vector,
    MixedIntegerLinearProgram,
    QQ,
    zero_vector,
    ZZ
)
from sage.rings.polynomial.polydict import ETuple

gc.enable()

DIR_PATH = os.path.dirname(os.path.realpath(__file__))


def get_logger(model_path: str, suffix: str = "") -> logging.Logger:
    """
    Construct logger and store

    :param model_path: str, path for logger output
    :param name_suffix: str, path for logger output
    :return logger object
    """
    if suffix == "":
        now_string = f'{dt.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")}'
        logger_fname = f'{model_path}_{now_string}.log'
    else:
        logger_fname = f'{model_path}_{suffix}.log'

    # create formatter
    log_format = '%(message)s'
    # log_format = '%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s'
    stream_formatter = logging.Formatter(log_format)

    # create file formatter
    file_formatter = logging.Formatter(log_format, datefmt='%Y-%m-%d %H:%M:%S')

    # create logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # fix for erroneous logger duplication
    logger.handlers = []

    # create file handler which logs even debug messages
    file_handler = logging.FileHandler(filename=logger_fname)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    # create console handler with a higher log level
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(stream_formatter)
    logger.addHandler(console_handler)

    return logger


def sage_ring(file_name):
    with open(file_name + "_varSage.txt", "r") as myfile:
        m2_var_string = myfile.read().rstrip('\n')

    m2_var_string = re.sub(r'[{}_]', '', m2_var_string)
    var_indices = eval(m2_var_string.replace('x', ''))

    R = QQ[m2_var_string]

    return R, var_indices


def fix_m2_list(m2_list_str):
    return m2_list_str.replace('{', '[').replace('}', ']')


def sage_fbases(file_name):
    with open(file_name + "_Fbases.txt", "r") as myfile:
        m2_fbases_string = myfile.read().rstrip('\n')

    fbases = dict(eval(fix_m2_list(m2_fbases_string)))

    return fbases


def sage_reduced_slack_matrix(R, file_name):
    with open(file_name + "_reducedSlackMatrix.txt", "r") as myfile:
        m2_slack_matrix_string = myfile.read().rstrip('\n')

    m2_slack_matrix_string = re.sub(r'(^{{|}}$|_)', '', m2_slack_matrix_string)

    slack_matrix = matrix(R, 
        [
            [R(m_ij) for m_ij in row.split(', ')]
            for row in m2_slack_matrix_string.split('}, {')
        ]
    )

    return slack_matrix


def sage_select_rows_columns(file_name):

    with open(file_name + "_SelRows_SelCols.txt", "r") as myfile:
        m2_rows_cols_string = myfile.read().rstrip('\n')

    select_rows, select_cols = eval(fix_m2_list(m2_rows_cols_string))

    return select_rows, select_cols


def sage_orientation(file_name, redundant_facets="false"):
    if redundant_facets == "true":
        orientation_file_name = file_name + "_orientation_redundant_facets.txt"
    else:
        orientation_file_name = file_name + "_orientation.txt"

    if not os.path.isfile(orientation_file_name):
        return dict()

    with open(orientation_file_name, "r") as myfile:
        m2_orientation_str = myfile.read().rstrip('\n')

    return dict(eval(fix_m2_list(m2_orientation_str)))


def sage_poly_list(R, full_file_name):
    with open(full_file_name + "_polySage.txt", "r") as myfile:
        m2_poly_string = myfile.read().rstrip('!}\n')

    m2_poly_string = re.sub(r'[{}_]', '', m2_poly_string)

    poly_dict = {
        R(substr.split(':')[0]): eval(substr.split(':')[1])
        for substr in m2_poly_string.split('!, ')
    }

    return poly_dict


class NonRealizabilityTest:

    def __init__(
        self,
        check_certificate_m2="false",
        dimension=4,
        deep_sign_check="false",
        facets="{}",
        file_name="Altshuler_N10_3574",
        flag="{}",
        force_new_flag="false",
        gurobi_interface="matlab",
        include_rows="{}",
        language="m2",
        load_data="false",
        matrix_method="sphere_new",
        max_deg=2,
        n_factors=2,
        oriented="false",
        prismatoid="false",
        random_seed=0,
        redundant_facets="false",
        selected_cols="{}",
        selected_rows="{}",
        shuffle_constraints="false",
        sleep=3,
        small_determinants="false",
        suffix="",
        switch_to_sage="false",
    ):
        self.n_factors = n_factors
        self.max_deg = max_deg

        self.check_certificate_m2 = check_certificate_m2
        self.deep_sign_check = deep_sign_check
        self.oriented = oriented
        self.small_determinants = small_determinants
        self.prismatoid = prismatoid
        self.dimension = dimension
        self.force_new_flag = force_new_flag
        self.load_data = load_data
        self.language = language
        self.redundant_facets = redundant_facets
        self.switch_to_sage = switch_to_sage

        self.facets = facets
        self.selected_cols = selected_cols
        self.selected_rows = selected_rows
        self.include_rows = include_rows
        self.flag = flag

        self.shuffle_constraints = shuffle_constraints

        self.file_name_old = file_name
        self.suffix = suffix
        if self.prismatoid == "true" or matrix_method.startswith("prismatoid"):
            self.file_name = f'Criado_Santos_P{file_name}'
        else:
            self.file_name = file_name

        self.full_file_name = os.path.join(DIR_PATH, f'{self.file_name}_iter{self.n_factors}_deg{self.max_deg}')

        self.logger = get_logger(self.full_file_name+"_output", suffix=self.suffix)

        if self.suffix:
            self.full_file_name += f'_{self.suffix}'

        if matrix_method in {"sphere_from_data", "sphere", "prismatoid_from_data", "prismatoid", "sphere_new"}:
            self._generate_constraints = getattr(self, f"matrix_{matrix_method}")
        else:
            raise NotImplementedError(
                'interface not implemented. '
                'Insert one of "sphere_from_data", "sphere", "sphere_new", "prismatoid_from_data", "prismatoid".'
            )

        self.gurobi_interface = gurobi_interface
        self.random_seed = random_seed
        random.seed(self.random_seed)
        self.R = None
        self.sleep = sleep

    def matrix_sphere_new(self):
        return (
            f'matrixSphereNew("{self.file_name_old}", {self.n_factors}, {self.max_deg}, '
            f'DeepSignCheck => {self.deep_sign_check}, '
            f'Dimension => {self.dimension}, '
            f'Facets => {self.facets}, '
            f'ForceNewFlag => {self.force_new_flag}, '
            f'LoadData => {self.load_data}, '
            f'Prismatoid => {self.prismatoid}, '
            f'Oriented => {self.oriented}, '
            f'SmallDeterminants => {self.small_determinants}, '
            f'SelectedColumns => {self.selected_cols}, '
            f'SelectedRows => {self.selected_rows}, '
            f'IncludeRows => {self.include_rows}, '
            f'RedundantNonsimplicialFacets => {self.redundant_facets}, '
            f'Flag => {self.flag}, '
            f'RandomSeed => {self.random_seed}, '
            f'Suffix => "{self.suffix}", '
            f'SwitchToSage => {self.switch_to_sage}, '
            f'DumpSage => true'
            ');'
        )

    @staticmethod
    def _flatten(nested_list):
        return list(chain.from_iterable(nested_list))

    @staticmethod
    def partition_factors(predicate, iterable):
        """
        Source: https://giannitedesco.github.io/2020/12/14/a-faster-partition-function.html
        """
        ts, fs = [], []
        t = ts.append
        f = fs.append
        for q, e in iterable:
            (t if predicate(q) else f)(q ** e)
        return ts, fs

    def sat_poly(self, f, coord_pair, sign_C):
        """
        -- INPUT: polynomial f
        -- OUTPUT: f divided by any (positive) monomial factors
        -- Note: the constant factor still appears in the result
        """
        if self.R(f).degree() == 0:
            return (self.R(1), coord_pair, sign_C), self.R(f)

        factors = f.factor()
        monomial_part, simplified_f = self.partition_factors(
            predicate=lambda q: q.number_of_terms() == 1 and q.degree() != 0,
            iterable=factors
        )
        return (product(monomial_part), coord_pair, sign_C), product(simplified_f + [factors.unit()])

    def find_first_sign(self, col, pos_sat):
        """
        -- INPUT: col = list of polynomials (in a certain column of slack matrix)
        --     pos_sat = list of polynomials already determined to be positive
        -- OUTPUT: if there exist g in col and p in posSat such that g % p == 0
        --     and g//p is a monomial, it returns the sign of g//p; otherwise it returns 0
        """
        for g in col:
            for p in pos_sat:
                quot = g // p
                if (g % p == 0) and quot.number_of_terms() == 1:
                    return self.sat_poly(quot, None, None)[1]
        return 0

    def find_sign(self, g, pos_sat):
        """
        -- INPUT: g = polynomial (in a certain column of slack matrix)
        --     pos_sat = list of polynomials already determined to be positive
        -- OUTPUT: if there exist g in col and p in posSat such that g % p == 0
        --     and g//p is a monomial, it returns the sign of g//p; otherwise it returns 0
        """
        for p in pos_sat:
            quot = g // p
            if (g % p == 0) and quot.number_of_terms() == 1:
                return self.sat_poly(quot, None, None)[1]
        return 0

    def pos_check(self, hash_M, rest_cols, pos_sat_hash, orientation):
        """
        -- INPUT: hash_M = hashTable whose keys are pairs of indices of a matrix M
        --                and values are nonzero entries of M
        --     rest_cols = columns left to check for sign
        --     pos_sat_hash = hash table of polynomials already determined to be positive
        --     orientation = list containing facets orientation (as pairs (col index, sign +1/-1))
        -- OUTPUT: rest_cols = columns whose sign are left to determine
        --    pos_sat_hash = list of polynomials entries determined to be > 0)
        --    pos_C = list of columns whose sign we determined
        --    orientation = updated orientation list
        """
        pos_C = set()
        for c in rest_cols:
            hash_col_C = {(i, j): v for (i, j), v in hash_M.items() if j == c}

            if self.deep_sign_check == "true":
                sign_C = {
                    self.find_sign(g, set(pos_sat_hash.values())) for g in hash_col_C.values()
                }
                if {1, -1}.issubset(sign_C):
                    self.logger.info(f"Conflicting signs: {sign_C}")
                    raise Exception(f"\nFound incompatible signs in column {c}")
            else:
                sign_C = {
                    self.find_first_sign(hash_col_C.values(), set(pos_sat_hash.values()))
                }
            # do we need _flatten(pos_sat_hash.values()) ??
            if sign_C == {0}:
                continue

            unique_sign_C = (sign_C - {0}).pop()

            orientation.update({c: unique_sign_C})
            # otherwise append index c to the list of columns for which we set the sign
            pos_C.add(c)

            # we add to posSatHash the unique elements in posPolyC multiplied
            # by sign and divided by possible monomial factors
            new_hash_col_C = dict(
                (
                    self.sat_poly(unique_sign_C * v, coord_pair, unique_sign_C)
                    for coord_pair, v in hash_col_C.items()
                )
            )
            pos_sat_hash.update(new_hash_col_C)
        rest_cols -= pos_C
        return rest_cols, pos_sat_hash, pos_C, orientation

    def col_signs(self, fbases, slack_matrix, include_rows, select_rows, select_cols):
        """
        -- INPUT: Fbases = list of facet bases
        --        M = reconstructed slack matrix
        --        fileName = string, name of file
        -- OUTPUT: list posSat of polynomial entries in the reconstructed matrix with correct signs
        """
        fl = eval(self.flag)
        rest_cols = set(range(len(fbases)))

        # hashTable whose keys are pairs of indices and values are nonzero entries of M
        r, c = slack_matrix.nrows(), slack_matrix.ncols()
        hash_M = {(i, j): slack_matrix[i,j] for i, j in iter_prod(range(r), range(c)) if slack_matrix[i,j] != 0}

        self.logger.info("\nCheck for reconstructed columns containing monomials.")
        # load orientation if given
        if self.oriented == "false":
            orientation = dict()
        else:
            orientation = sage_orientation(file_name=self.file_name, redundant_facets=self.redundant_facets)
        if len(orientation) > 0:

            self.logger.info("\nOrientation file found.")

            start = time.time()
            for c in rest_cols:
                hash_col_C = {(i, j): v for (i, j), v in hash_M.items() if j == c}

                mons = {k: v for k, v in hash_col_C.items() if v.number_of_terms() == 1}
                # if there are no monomials in the column c, then check another column
                if len(mons) == 0:
                    continue

                # compute signs of monomials and check they have same sign
                unique_signs = list({self.sat_poly(m, None, None)[1] for m in mons.values()})
                if not(all(s > 0 for s in unique_signs) or all(s < 0 for s in unique_signs)):
                    self.logger.info(f"Conflicting signs: {unique_signs}")
                    raise Exception(f"\nFound incompatible signs (from monomials) in column {c}")

                # sign of column c in given orientation
                sign_C = orientation[c]
                if unique_signs[0] != sign_C:
                    self.logger.info("Switching signs to the input orientation")
                    orientation = {k: -v for k, v in orientation.items()}

                break

            end = time.time()
            self.logger.info(f' -- {end - start:,.6f} seconds elapsed')

            self.logger.info('Creating pos_sat_hash')
            start = time.time()
            pos_sat_hash = dict(
                self.sat_poly(orientation[j] * v, (i, j), orientation[j]) 
                for (i, j), v in hash_M.items() if (
                    i in select_rows and j in select_cols
                    and include_rows.issubset(set(fbases[j]).union({i}))
                    and v.degree() <= int(self.max_deg)
                )
            )
            self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')

            self.logger.info(f"\nOrientation : {sorted(list(orientation.items()))}")

            # selecting constraints contained in the flag columns divided by monomial factors
            self.logger.info("\nRemultiplying by monomial factors.")
            if self.small_determinants == "true":
                return {
                    ((i, j), k2): v if j in fl else k0 * v
                    for (k0, (i, j), k2), v in pos_sat_hash.items()
                }
            return {(k1, k2): k0 * v for (k0, k1, k2), v in pos_sat_hash.items()}
            
        # the orientation is not given
        start = time.time()
        pos_sat_hash = dict()
        pos_C = set()
        for c in rest_cols:
            hash_col_C = {(i, j): v for (i, j), v in hash_M.items() if j == c}

            mons = {k: v for k, v in hash_col_C.items() if v.number_of_terms() == 1}

            # if there are no monomials in the column c, then check another column
            if len(mons) == 0:
                continue

            # compute signs of monomials and check they have same sign
            unique_signs = list({self.sat_poly(m, None, None)[1] for m in mons.values()})
            if not(all(s > 0 for s in unique_signs) or all(s < 0 for s in unique_signs)):
                self.logger.info(f"Conflicting signs: {unique_signs}")
                raise Exception(f"\nFound incompatible signs (from monomials) in column {c}")

            sign_C = unique_signs[0]
            orientation.update({c: sign_C})

            # we get here if all monomials have the same sign
            pos_C.add(c)

            # we add to posSatHash the unique elements in hash_col_C multiplied by sign_C 
            # and divided by possible monomial factors

            new_hash_col_C = dict(
                self.sat_poly(sign_C * v, coord_pair, sign_C)
                for coord_pair, v in hash_col_C.items()
            )

            pos_sat_hash.update(new_hash_col_C)

        rest_cols -= pos_C
        end = time.time()
        self.logger.info(f' -- {end - start:,.6f} seconds elapsed')

        self.logger.info("\nOrientation file not found.")

        self.logger.info(f"\nIndices of columns containing monomials: {sorted(set(range(len(fbases))) - rest_cols)}")
        self.logger.info(f"Indices of columns with no monomials: {rest_cols}")

        self.logger.info("\nTest remaining reconstructed columns against polynomials in pos_sat_hash.")

        # Test remaining reconstructed columns for elements that now already
        # occur in the positive polynomial list.
        # Add these entries with appropriate sign to list of positive polynomials
        pos_C = set({-1})
        start = time.time()

        while len(rest_cols) != 0 and len(pos_C) != 0:
            rest_cols, pos_sat_hash, pos_C, orientation = self.pos_check(
                hash_M,
                rest_cols,
                pos_sat_hash,
                orientation
            )
            self.logger.info(f"List of columns left to fix: {rest_cols}")

        end = time.time()
        self.logger.info(f' -- {end - start:,.6f} seconds elapsed')

        if self.redundant_facets == "true":
            orientation_file_name = f"{self.file_name}_orientation_redundant_facets.txt"
        else:
            orientation_file_name = f"{self.file_name}_orientation.txt"
        self.logger.info(f"Dumping orientation to file {orientation_file_name}")

        with open(orientation_file_name, 'w') as f:
            f.write(str(set(orientation.items())))

        if len(rest_cols) > 0:
            self.logger.info("Warning: rest_cols is non-empty after fixing the column signs.")

        self.logger.info(f"\nOrientation : {sorted(list(orientation.items()))}")

        # selecting constraints contained in the flag columns divided by monomial factors
        self.logger.info("\nRemultiplying by monomial factors.")
        if self.small_determinants == "true":
            return {
                ((i, j), k2): v if j in fl else k0 * v
                for (k0, (i, j), k2), v in pos_sat_hash.items() if (
                    i in select_rows and j in select_cols
                    and include_rows.issubset(set(fbases[j]).union({i}))
                    and v.degree() <= int(self.max_deg)
                )
            }
        return {
            ((i, j), k2): k0 * v for (k0, (i, j), k2), v in pos_sat_hash.items() if (
                i in select_rows and j in select_cols
                and include_rows.issubset(set(fbases[j]).union({i}))
                and v.degree() <= int(self.max_deg)
            )
        }

    @staticmethod
    def _split_standard_form(poly_dict, n_vars):
        constant_term = poly_dict.pop(ETuple([0] * n_vars), 0)
        return constant_term, poly_dict

    def _get_products_step(self, constr_dict, h, new_constraint_dict = None, last_iter = None):
        constr_list = list(constr_dict.keys())
        if h == 2:
            stratified_constrs = [
                [
                    (
                        constr_list[i] * Cj,
                        [
                            constr_dict[constr_list[i]][0]+constr_dict[Cj][0],
                            constr_dict[constr_list[i]][1]*constr_dict[Cj][1]
                        ]
                    ) for i in range(j+1)
                ]
                for j, Cj in enumerate(constr_list)
            ]
            new_constraint_dict = cp.copy(constr_dict)
        else:
            stratified_constrs = [
                self._flatten(
                    [
                        [
                            (
                                p * Ck,
                                [
                                    new_constraint_dict[p][0]+constr_dict[Ck][0],
                                    new_constraint_dict[p][1]*constr_dict[Ck][1]
                                ]
                            ) for p, _ in last_Cj
                        ] for last_Cj in last_iter[:k+1]
                    ]
                )
                for k, Ck in enumerate(constr_list)
            ]
        constrs = dict(self._flatten(stratified_constrs))

        self.logger.info(f"#unique products of {h} constraints: {len(constrs)}")
        new_constraint_dict.update(constrs)
        return new_constraint_dict, stratified_constrs

    def get_products(self, constraint_dict):
        if not self.n_factors in {1, 2, 3, 4, 5}:
            raise Exception("n_factors > 5 not supported.")

        self.logger.info(f"#constraints up to deg {self.max_deg}: {len(constraint_dict)}")
        if self.n_factors == 1:
            return constraint_dict

        new_constraint_dict, last_iter = None, None
        for current_n_factors in range(2, 6):
            new_constraint_dict, last_iter = self._get_products_step(
                constraint_dict,
                current_n_factors,
                new_constraint_dict,
                last_iter
            )
            gc.collect()
            if self.n_factors == current_n_factors:
                return new_constraint_dict

    def get_coefficient_matrix(self, constraint_dict):
        # constraint_dict = dict(sorted(constraint_dict.items(), key=lambda item: item[1][0]))
        constraint_list = constraint_dict.keys()
        self.logger.info("Creating standard form of polynomials.")
        start = time.time()
        standard_forms = [p.dict() for p in constraint_list]
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info("\nIsolating constant terms.")
        start = time.time()
        constant_terms, split_standard_forms = list(
            zip(
                *[self._split_standard_form(sfp, n_vars=len(self.R.gens())) for sfp in standard_forms]
            )
        )
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info("\nCreating lists of monomials, coefficients, and constant terms.")
        start = time.time()
        list_mons_per_poly, sparse_matrix_entries_list = (
            [list(sfp.keys()) for sfp in split_standard_forms],
            [list(sfp.values()) for sfp in split_standard_forms]
        )
        sparse_matrix_entries = self._flatten(sparse_matrix_entries_list)
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info("\nCreating list of unique monomials.")
        start = time.time()
        set_mons = set(self._flatten(list_mons_per_poly))
        dict_mons = {mon: j+1 for j, mon in enumerate(set_mons)}
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info("\nGenerating row and column indices for sparse matrix.")
        start = time.time()
        sparse_matrix_rows = self._flatten([[i+1] * len(p) for i, p in enumerate(list_mons_per_poly)])
        # sparse_matrix_cols_list = [[dict_mons[m] for m in mp] for mp in list_mons_per_poly]
        sparse_matrix_cols = self._flatten([[dict_mons[m] for m in mp] for mp in list_mons_per_poly])
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info("\nAppending constant term entries.")
        start = time.time()
        if any(v != 0 for v in constant_terms):  # sum(constant_terms) > 0:
            indices_constant_terms, nonzero_constant_terms = list(zip(*[(i+1, c) for i, c in enumerate(constant_terms) if c != 0]))
            sparse_matrix_rows += indices_constant_terms
            sparse_matrix_cols += [len(set_mons)+1] * len(indices_constant_terms)
            sparse_matrix_entries += nonzero_constant_terms
        else:
            sparse_matrix_rows += [1]
            sparse_matrix_cols += [len(set_mons)+1]
            sparse_matrix_entries += [0]
            self.logger.info(f"Warning: no constant term column, adding a dummy 0 entry in position (1, {len(set_mons)+1}).")
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info(f"\nDumping sparse matrix to file {self.full_file_name}_matrix.txt")
        start = time.time()
        with open(f"{self.full_file_name}_matrix.txt", 'w') as outcsv:
            writer = csv.writer(outcsv, delimiter=' ', lineterminator='\n')
            for el in [sparse_matrix_rows, sparse_matrix_cols, sparse_matrix_entries]:
                writer.writerow(el)
        end = time.time()
        self.logger.info(f" -- {end - start:,.6f} seconds elapsed")

        self.logger.info(f"\n-> Number of rows: {len(list_mons_per_poly):,}")
        self.logger.info(f"-> Number of columns: {len(set_mons)+1:,}")
        self.logger.info(f"-> Number of non-zero entries: {len(sparse_matrix_rows):,}")

        return sparse_matrix_rows, sparse_matrix_cols, sparse_matrix_entries

    def solve_lp(self, matrix_fname=None, M=None):
        if M is None:
            matrix_file_name = matrix_fname or f'{self.full_file_name}_matrix.txt'
            M = np.loadtxt(matrix_file_name).astype(np.int32)

        r = np.max(M[0])
        c = np.max(M[1])

        #self.logger.info("Defining sparse matrix")
        B = matrix(ZZ, {(M0i-1, M[1][i]-1): M[2][i] for i, M0i in enumerate(M[0])}, sparse=True)

        last_col = matrix(ZZ, r, 1, {(i, 0): 1 for i in range(r)}, sparse=True)

        #self.logger.info("Matrix D")
        D = B.augment(last_col).transpose()

        #self.logger.info("LP")
        p = MixedIntegerLinearProgram(maximization=False, solver='Gurobi')#GurobiBackend)
        p.solver_parameter("Method", 0)
        p.solver_parameter("Threads", 8)

        y = p.new_variable(indices=range(r))
        p.set_max(y, 0)

        p.add_constraint(B.transpose() * y == zero_vector(c))
        p.add_constraint(sum(y) >= -1)

        p.set_objective(sum(y)+1)

        p.solve(log=3)

        sol = np.fromiter(p.get_values(y).values(), dtype=float)
        self.logger.info(f"Objective value: {sum(sol)+1}")

        slacks = np.dot(D, sol) - vector(list(zero_vector(c))+[-1])
        self.logger.info(f"Are there zero slacks? {any(slacks==0)}")

        idx_nonzero_dual_vars = np.nonzero(sol)[0]
        self.logger.info(f"Indices of nonzero dual variables : {[m+1 for m in idx_nonzero_dual_vars]}")

        sparse_subD = D[:-2, list(idx_nonzero_dual_vars)]

        constant_terms_subD =  np.array(D[-2, list(idx_nonzero_dual_vars)])

        # transform sparse_subD into numpy array
        numpy_subD = np.array(sparse_subD)

        # a = array with row indices of nonzero elements, i.e. indices of monomials appearing in sparse_subD
        # b = array with column indices of nonzero elements
        a, b = np.nonzero(numpy_subD)
        unique_mons_index = list(np.unique(a))
        mons_index = unique_mons_index + [c-1]
        self.logger.info(f"Column indices : {[m+1 for m in mons_index]}")

        certificate_matrix = matrix(
            np.concatenate(
                (numpy_subD[unique_mons_index, :].transpose(), constant_terms_subD.reshape(-1, 1)),
                axis=1
            )
        )

        self.logger.info(f"Submatrix of nonzero primal constraints :\n{certificate_matrix}")
        self.logger.info(f"Sum of nonzero primal constraints :\n{sum(certificate_matrix.rows())}")

        str_certificate = " ".join([str(c+1) for c in mons_index])
        for i in range(len(np.unique(b))):
            mons_constr_cert = " ".join(
                [str(c) for c in certificate_matrix[i]]
            )
            str_certificate += f"\n{mons_constr_cert}"

        with open(self.full_file_name + "_certificate.txt", "w") as text_file:
            _ = text_file.write(str_certificate)

        with open(self.full_file_name + "_certificate_rows.txt", "w") as text_file:
            _ = text_file.write(" ".join(idx_nonzero_dual_vars))

    def check_certificate(self, constraint_dict, fbases):
        certificate_row = np.loadtxt(self.full_file_name + "_certificate_rows.txt").astype(np.int32)

        certificate_pairs = [(k, [i+1]+v) for i, (k, v) in enumerate(constraint_dict.items()) if i+1 in certificate_row]
        certificate, coordinates = list(zip(*certificate_pairs))

        certificate_coords = [list(np.array(coord)+1) for _, coord, _ in coordinates]

        # self.logger.info("\nCertificate:")
        # for p, (i, lst, sign) in certificate_pairs:
        #     plus_one_list = list(map(tuple, np.array(lst) + 1))
        #     self.logger.info(f"    {[i, plus_one_list, sign]}\t=>\t{p}")

        with open(self.full_file_name + "_finalCertificate_sage.txt", "w") as out_file:
            out_file.write(
                f"""{
                    [list(map(tuple, a)) for a in certificate_coords]
                }""".replace('[', '{').replace(']', '}')
            )

        self.logger.info(f"\nNumber of constraints in certificate: {len(certificate)}")
        self.logger.info(f"Slack certificate (with indices starting from 1!):")
        subset_fbases = dict()
        for _, (i, lst, sign) in certificate_pairs:
            plus_one_list = list(map(tuple, np.array(lst) + 1))
            subset_fbases.update({i+1: list(np.array(fbases[i]) + 1) for i in list(zip(*lst))[1]})
            self.logger.info(f"    {[i, plus_one_list, sign]}")

        self.logger.info(
            f"\nFacets in certificate (with indices starting from 1!):\n{sorted(subset_fbases.items())}"
        )

        if sum(certificate) == 0:
            self.logger.info("\nThe certificate sums to 0!")
        else:
            self.logger.info("\nThe certificate does NOT sum to 0.")

    @property
    def relabel_data(self):
        return (
            f'relabelData("{self.file_name}", {self.n_factors}, {self.max_deg}, '
            f'RedundantNonsimplicialFacets => {self.redundant_facets}'
            ');'
        )

    @property
    def run(self):
        self.logger.info("\n---- GENERATING CONSTRAINTS ----")
        self.logger.info(f"Random seed for shuffling: {self.random_seed}")

        if self.language == "m2":
            macaulay2.eval(f'load("{DIR_PATH}/reconstruction_slack_matrix_and_certificate.m2")')
            m2_input_str = self._generate_constraints()
            self.logger.info(f"Executing M2 command:\n{m2_input_str}\n")
            self.logger.info(macaulay2.eval(m2_input_str))

            self.R, var_indices = sage_ring(self.file_name)
            fbases = sage_fbases(self.file_name)
        elif self.language == "sage":
            raise NotImplementedError("Interface with only sage not implemented. Please use m2.")
        else:
            self.logger.info("Skipping constraint generation in macaulay2.")

        if self.switch_to_sage == "true" or self.language == "sage":
            self.logger.info("\n---- SWITCHING TO SAGE ----")
            S1 = sage_reduced_slack_matrix(self.R, self.file_name)

            self.logger.info("Reconstructing slack matrix")
            start = time.time()
            slack_matrix = matrix(self.R,
                [
                    [det(S1[list(fbases[j])+[i], :]) for j in range(len(fbases))]
                    for i in range(S1.nrows()) 
                ]
            )
            
            self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')
            
            start = time.time()

            include_rows = set(eval(fix_m2_list(self.include_rows)))
            select_rows, select_cols = sage_select_rows_columns(self.file_name)

            raw_constraint_dict = self.col_signs(
                fbases, slack_matrix, include_rows, select_rows, select_cols
            )
            self.logger.info("\nTotal time for col_signs:")
            self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')

            constraint_dict = {
                v: [[(i, j)], sign]
                for ((i, j), sign), v in raw_constraint_dict.items() if v != 1
            }

            self.logger.info(f"Total number of constraints up to degree {self.max_deg}: {len(constraint_dict)}")

            if len(constraint_dict) == 0:
                raise Exception(f"No constraints up to degree {self.max_deg}")
        else:
            constraint_dict = sage_poly_list(self.R, self.full_file_name)

        gc.collect()

        self.logger.info("\n\n---- GENERATING PRODUCTS ----")
        self.logger.info(f"#variables in S1 : {len(self.R.gens())}")
        start = time.time()
        new_constraint_dict = self.get_products(constraint_dict)

        self.logger.info("Appending variables in S1.")
        new_constraint_dict.update(
            {
                var: [[(-var_indices[i], -var_indices[i])], 1]
                for i, var in enumerate(self.R.gens())
            }
        )

        if self.shuffle_constraints == 'true':
            self.logger.info("Shuffling constraints.")
            new_constraint_list = list(new_constraint_dict.items())
            random.shuffle(new_constraint_list)
            new_constraint_dict = dict(new_constraint_list)
        self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')
        self.logger.info(
            f"Total number of constraints of degree <= {self.max_deg} "
            f"and their products up to {self.n_factors} factors: {len(new_constraint_dict)}\n"
        )
        gc.collect()

        self.logger.info("\n---- GENERATING COEFFICIENT MATRIX ----")
        start = time.time()
        coefficient_matrix = self.get_coefficient_matrix(new_constraint_dict)
        self.logger.info("\nTotal time for generating coefficient matrix.")
        self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')
        gc.collect()

        if (
            os.path.isfile(self.full_file_name + "_certificate_rows.txt") and 
            self.gurobi_interface != 'skip'
        ):
            self.logger.info("\nRemoving old certificate file.")
            os.remove(self.full_file_name + "_certificate_rows.txt")

        if self.gurobi_interface == 'skip':
            self.logger.info("\n---- SKIPPING LP RESOLUTION ----")
        elif self.gurobi_interface == 'sage':
            self.logger.info("\n---- RUNNING GUROBI VIA SAGE ----")
            start = time.time()
            self.solve_lp(M=coefficient_matrix)
            self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')
        elif self.gurobi_interface == 'matlab':
            matrix_file_name = f"{self.full_file_name}_matrix.txt".split('/')[-1]

            while not (os.path.isfile(matrix_file_name) and os.stat(matrix_file_name).st_size > 1):
                self.logger.info(f"File {matrix_file_name} not found yet, waiting {self.sleep} seconds...")
                time.sleep(self.sleep)
            
            self.logger.info("\n---- RUNNING GUROBI VIA MATLAB ----")
            true_logger_fname = self.logger.handlers[0].baseFilename.split('/')[-1]
         
            start = time.time()
            self.logger.info(f"Matrix file name: {matrix_file_name}")
            os.system(
                f"sh solve_lp_gurobi_matlab.sh 0 yes {matrix_file_name} {true_logger_fname} {self.sleep}")
            while os.path.isfile(f"{self.full_file_name}.m"):
                time.sleep(self.sleep)
            self.logger.info(f' -- {time.time() - start:,.6f} seconds elapsed')
        else:
            raise NotImplementedError('Currently supported Gurobi interfaces: "matlab", "sage".')

        if (
            os.path.isfile(self.full_file_name + "_certificate_rows.txt") and
            os.stat(self.full_file_name + "_certificate_rows.txt").st_size > 1
        ):
            self.logger.info("\n---- CHECKING CERTIFICATE ----")
            print("Adding index to dictionary.")
            self.logger.info(f"\nFile {self.full_file_name}_certificate_rows.txt found, checking...")
            self.check_certificate(new_constraint_dict, fbases)
            
            print("\nCreating final data file.")
            self.logger.info(macaulay2.eval(self.relabel_data))

            if self.check_certificate_m2 == "true":
                self.logger.info("\n---- SWITCHING TO M2 ----")
                m2_input_str = f'checkCertificate("{self.file_name}", {self.n_factors}, {self.max_deg}, Prismatoid => {self.prismatoid})'
                self.logger.info(f"Executing M2 command:\n{m2_input_str}\n")
                self.logger.info(macaulay2.eval(m2_input_str))
            self.logger.info("\n---- DONE ----")
            return 0
        self.logger.info(f"\nFile {self.full_file_name}_certificate_rows.txt not found.")

        return 1


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser()

    PARSER.add_argument('--check_certificate_m2', '-ccm2', type=str, default="false")
    PARSER.add_argument('--deep_sign_check', '-deep', type=str, default="false")
    PARSER.add_argument('--dimension', '-d', type=int, default="4")
    PARSER.add_argument('--facets', '-f', default="{}")
    PARSER.add_argument('--file_name', '-n', type=str, default="Altshuler_N10_3574")
    PARSER.add_argument('--flag', '-fl', default="{}")
    PARSER.add_argument('--force_new_flag', '-nf', type=str, default="false")
    PARSER.add_argument('--gurobi_interface', '-g', type=str, default="matlab")
    PARSER.add_argument('--include_rows', '-i', default="{}")
    PARSER.add_argument('--language', '-lang', type=str, default="m2")
    PARSER.add_argument('--load_data', '-data', type=str, default="false")
    PARSER.add_argument('--matrix_method', '-m', type=str, default="sphere_new")
    PARSER.add_argument('--max_deg', '-l', type=int, default=2)
    PARSER.add_argument('--n_factors', '-k', type=int, default=2)
    PARSER.add_argument('--oriented', '-o', type=str, default="false")
    PARSER.add_argument('--prismatoid', '-p', type=str, default="false")
    PARSER.add_argument('--random_seed', '-rs', type=int, default=0)
    PARSER.add_argument('--redundant_facets', '-rf', type=str, default="false")
    PARSER.add_argument('--selected_cols', '-c', default="{}")
    PARSER.add_argument('--selected_rows', '-r', default="{}")
    PARSER.add_argument('--shuffle_constraints', '-sc', type=str, default="false")
    PARSER.add_argument('--sleep', '-slp', type=int, default=3)
    PARSER.add_argument('--small_determinants', '-sd', type=str, default="false")
    PARSER.add_argument('--switch_to_sage', '-ss', type=str, default="true")
    PARSER.add_argument('--suffix', '-s', type=str, default="")

    ARGS = vars(PARSER.parse_args())

    NonRealizabilityTest(**ARGS).run
