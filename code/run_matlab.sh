
fname=$1

if [ -z "$1" ]; then
    echo "No .m script name supplied. Please specify a file name.";
fi

command="run('"${fname}"');exit;"
echo 'Running script '${fname}'...'
echo 'Command: '${command}'...'

matlab -nodisplay -nosplash -nodesktop -r ${command} | tail -n +11
