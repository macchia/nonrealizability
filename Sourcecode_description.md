# **Searching for non-realizability certificates of combinatorial polytopes**

The files in the code folder are attached to the paper "GENERAL NON-REALIZABILITY CERTIFICATES FOR SPHERES WITH LINEAR PROGRAMMING" by João Gouveia, Antonio Macchia and Amy Wiebe, preprint (2021), [arXiv:2109.15247](https://arxiv.org/abs/2109.15247), to which we refer to for further details.

## **WARNING!**
We remark that the current implementation of the algorithm is still experimental and far from being optimized.

## **Dependencies**

The following dependencies are needed to execute this code: 

* [SageMath](https://www.sagemath.org/), 
* [Macaulay2](http://www2.macaulay2.com/Macaulay2/), 
* [Gurobi](https://www.gurobi.com/), 
* [MATLAB](https://www.mathworks.com/products/matlab.html). 

Please make sure that the user can execute Gurobi via MATLAB (for more details, see [here](https://www.gurobi.com/documentation/9.5/quickstart_windows/matlab_setting_up_grb_for_.html)).

The algorithm uses the `Macaulay2` library [SlackIdeals](http://www2.macaulay2.com/Macaulay2/doc/Macaulay2-1.16/share/doc/Macaulay2/SlackIdeals/html/index.html), developed by Antonio Macchia and Amy Wiebe. For further details, see also the paper [Slack Ideals in Macaulay2](https://link.springer.com/chapter/10.1007%2F978-3-030-52200-1_22), in *Mathematical software – ICMS 2020*, A. Bigatti, J. Carette, J. Davenport, M. Joswig, T. de Wolff (eds.), pages 222-231, Lecture Notes in Computer Science, vol. 12097, Springer, Cham, 2020.

## **How to run**

* Move to the folder containing the source code:
```
cd code
```
* To test that the code is functional, please run the following command:
```
sage nonrealizability_certificate.py -n Altshuler_N10_3574 -k 2 -l 2
```
The user should see a terminal output ending with `---- DONE ----`.

## **Main function arguments**

The Boolean arguments take value `"true"` or `"false"` (written as strings).


### **Mandatory arguments**

* `--file_name`, `-n`, (type=str), there are two types of file names for a combinatorial polytope: 

    - the user can specify the name `<filename>` of a combinatorial polytope *P* such that:
      - if `load_data` is `"true"`, the algorithm looks for a file `<filename>_data.txt` having the same structure of the file [Altshuler_N10_3574_data.txt](https://bitbucket.org/macchia/nonrealizability/src/master/code/Altshuler_N10_3574_data.txt). More precisely, this file is a tuple in `Macaulay2` format containing

        1. list of facets of *P* given as lists of vertex labels
        2. dimension *d* of *P*
        3. flag *Fl* as list of *d+1* facet labels
        4. polynomial ring in the variables of the symbolic slack matrix of *P*
        5. reduced symbolic slack matrix of *P* whose columns correspond to the facets of flag *Fl*, dehomogenized using a spanning tree *T* of the bipartite graph whose adjacency matrix has the same support as the full slack matrix of *P*. This matrix is uniquely determined by *Fl* and *T*
        6. spanning tree *T* used to dehomogenize the reduced symbolic slack matrix of *P*.

      - If `load_data` is `"false"`, then it relies on the arguments `dimension` and `facets`. In this case, the algorithm will recompute a flag, the corresponding reduced slack matrix and a spanning tree to dehomogenize the reduced slack matrix.

    - Otherwise the following names automatically retrieve the data specified above from the file `reconstruction_slack_matrix_and_certificate.m2`: 

        ```
        "Altshuler_N10_3574", "Doolittle_11v", "Doolittle_13v1", "Doolittle_13v2", "Novik_Zheng_12v", 
        "Firsching_F374225", "Firsching_T2775", "Zheng_16v", "Criado_Santos_P1039", "Criado_Santos_P1963", 
        "Criado_Santos_P2669", "Criado_Santos_P3513", "Criado_Santos_P2105".
        ```

* `--facets`, `-f`, (type: string), list of facets of *P* given as lists of vertex labels. This is not needed if `file_name` is already in the database of the file `reconstruction_slack_matrix_and_certificate.m2`, as specified above.

* `--dimension`, `-d`, (type: integer), dimension of combinatorial polytope.

* `--n_factors`, `-k`, (type: integer), maximum number of factors in the products of constraints.

* `--max_deg`, `-l`, (type: integer), maximum degree of constraints to consider.


### **Heuristic arguments**

* `--selected_rows`, `-r`, (type: string, default="{}"), list of vertex labels outside of a face *G* of *P*. The algorithm then searches for non-realizability certificates that only uses entries *S_{i,F}* of the parametrized slack matrix such that *(\{i\} ∪ F) ∩ G = ∅*. By default, this heuristic is not applied. For more details, refer to the **Vertex avoidance** paragraph in Section 5.4 of the paper.

* `--include_rows`, `-i`, (type: string, default="{}"), list of vertex labels of a face *G* of *P*. The algorithm then searches for non-realizability certificates that only uses entries *S_{i,F}* of the parametrized slack matrix such that *G ⊂ \{i\} ∪ F*. By default, this heuristic is not applied. For more details, refer to the **Vertex fixing** paragraph in Section 5.4 of the paper.

* `--small_determinants`, `-sd`, (type: string, default="false"), if `"true"`, the monomial factors of the reconstructed slack entries of the columns in the flag are simplified. By default, this heuristic is not applied. For more details, refer to the **Monomial simplification** paragraph in Section 5.4 of the paper.

* `--redundant_facets`, `-rf`, (type: string, default="false"), if `"true"` for any non-simplicial facet in the chosen flag, we also add redundant columns with all possible choices of facet bases. By default, the algorithm picks a single facet basis for any non-simplicial facet in the chosen flag. For more details, refer to the **Monomial simplification** paragraph in Section 5.4 of the paper.

* `--selected_cols`, `-c`, (type: string, default="{}"), list of facet labels *C*. The algorithm then searches for non-realizability certificates that only uses slack entries in the columns labeled by *C*. By default, this heuristic is not applied.


### **Optional arguments**

* `--flag`, `-fl`, (type: string, default="{}"), list of *d+1* facet labels that forms a flag of facets. By default, it retrieves the flag from other sources.

* `--force_new_flag`, `-nf`, (type: string, default="false"), if `"true"`, it computes a new flag, ignoring the loaded one, if any. By default, the algorithm uses the assigned flag.

* `--load_data`, `-data`, (type: string, default="false"), if `"true"`, it looks for a file called `<filename>_data.txt` and loads the data specified in the `file_name` argument description. By default, it recomputes the same data.

* `--oriented`, `-o`, (type: string, default="false"), if `"true"`, it looks for a file called `<filename>_orientation.txt` containing a list of pairs *(a, b)*, where *a* is a facet label and *b* is the sign of facet *a*. For example, the orientation can be produced and cached by previous runs of the algorithm. By default, this parameter is set to `"false"` and a new orientation will be computed. The orientation depends on the chosen flag.

* `--random_seed`, `-rs`, (type: int, default=0), integer that initializes the random number generator. Choosing different values for the random seed may produce: 1) a different flag if no flag was passed or `force_new_flag` is `"true"`; 2) a different spanning tree to dehomogenize the reduced slack matrix; 3) a different ordering of the constraints of the LP if `shuffle_constraints` is `"true"`.

* `--shuffle_constraints`, `-sc`, (type: string, default="false"), if `"true"` it randomly shuffles the constraints of the LP. This may produce different certificates according to the chosen random seed. In order to obtain a reproducible certificate, please specify a random seed.

* `--suffix`, `-s`, (type: string, default=""), string that specifies a suffix to be added to the name of the files produced by the algorithm. By default, no suffix is added.
