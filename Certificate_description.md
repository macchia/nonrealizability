# **Certificate description**

The files in the *nonrealizability_certificates* folder are attached to the paper "GENERAL NON-REALIZABILITY CERTIFICATES FOR SPHERES WITH LINEAR PROGRAMMING" by João Gouveia, Antonio Macchia and Amy Wiebe, preprint (2021), [arXiv:2109.15247](https://arxiv.org/abs/2109.15247), to which we refer to for further details.

Each file contains a data tuple necessary to reconstruct the non-realizability certificate of a certain combinatorial polytope P, where: 

 * the first element is a list of facets of combinatorial polytope P, given as lists of vertex labels

 * the second element is an integer, denoting the dimension of P

 * the third element is a list of facet labels, denoting a flag of facets of P

 * the fourth element is a dehomogenized reduced slack matrix of P whose columns correspond to the flag given before

 * the fifth element is a list of lists of pairs, which represents the non-realizability certificate. Each pair (i,j) represents the (i,j)-entry of the parametrized slack matrix, reconstructed starting from the reduced slack matrix given before. The certificate is obtained by taking the product of the reconstructed slack matrix entries in each list of pairs, and then taking the sum of all such products. The result is zero, contradicting the fact that the reconstructed slack matrix entries are strictly positive polynomials. 
